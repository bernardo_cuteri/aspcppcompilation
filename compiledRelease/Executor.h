#ifndef __EXECUTOR_H__
#define __EXECUTOR_H__

#include <map>
#include "WQi.h"
#include "RQi.h"
#include "AuxiliaryMap.h"
#include "Literal.h"
#include <iostream>
class Executor
{
public:
  Executor();
  /* use virtual otherwise linker will try to perform static linkage */
  virtual void executeProgram(const vector<Atom> & p);
  virtual void executeFromFile(const char* factsFile);
  virtual const std::vector<std::vector<Literal> > & getFailedConstraints() {
      return failedConstraints;
  }
private:
  std::vector<std::vector<Literal> > failedConstraints;

};

#endif
