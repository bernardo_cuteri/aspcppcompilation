/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SharedFunctions.h
 * Author: bernardo
 *
 * Created on January 18, 2017, 3:25 PM
 */

#ifndef SHAREDFUNCTIONS_H
#define SHAREDFUNCTIONS_H

#include <string>

bool isInteger(const std::string & s);
std::string escapeDoubleQuotes(const std::string & s);

#endif /* SHAREDFUNCTIONS_H */

