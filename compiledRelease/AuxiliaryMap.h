/* 
 * File:   AuxiliaryMap.h
 * Author: bernardo
 *
 * Created on March 7, 2016, 2:17 PM
 */

#ifndef AUXILIARY_MAP_H
#define	AUXILIARY_MAP_H
#include <list>
#include <vector>

using namespace std;

class AuxiliaryMap {
public:
    AuxiliaryMap();
    AuxiliaryMap(int height);
    AuxiliaryMap(int height, vector<int> * keyIndices);
    void insert2(vector<int>* value);
    virtual ~AuxiliaryMap();
    list<vector<int>*>* getValues(vector<int>& tuple);
protected:
    vector<AuxiliaryMap*> nestedArray[2];
    vector<list<vector<int>*>*> leafTuples[2];
    vector<int>* keyIndices;
    int height;
    int keySize;

};

#endif	/* AUXILIARYMAP_H */

