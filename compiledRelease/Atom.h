#ifndef ATOM_H
#define	ATOM_H
#include <string>
#include <vector>
using namespace std;

class Atom {
public:
    Atom();
    Atom(const string & predicateName, const vector<string> & terms);
    const string & getPredicateName() const;
    const string & getTermAt(int) const;
    int getTermsSize() const;
    const vector<string> & getTerms() const;
    int getAriety() const;
    bool isVariableTermAt(int) const;
    vector<int> getIntTuple() const;
    void print() const;
    string toString() const;
private:
    string predicateName;
    vector<string> terms;
};

#endif

