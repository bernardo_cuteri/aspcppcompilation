#include "Executor.h"

#include "ConstantsManager.h"

#include "DLV2libs/input/InputDirector.h"

#include "AspCore2InstanceBuilder.h"

extern "C" Executor* create_object() {
    return new Executor;
}
extern "C" void destroy_object( Executor* object ) {
    delete object;
}
Executor::Executor() {}

void performJoin_1(int index, WQi* wqi, RQi* rqi, const vector<int> & joinOrder, vector<int> * tuple0, AuxiliaryMap** auxiliaryMaps, vector<map<int, pair<int, int> > > & keys_indices, vector<AuxiliaryMap* > & atomMaps, const vector<bool> & negativeLiterals) {
    for(AuxiliaryMap* p: atomMaps) {
        p->insert2(tuple0);
    }
    if(negativeLiterals[index]) {
        return;
    }
    vector<int>* tupleStack[2];
    tupleStack[index]=tuple0;
    AuxiliaryMap* auxMap = auxiliaryMaps[0];
    vector<int> key_0;
    for(map<int, pair<int, int> >::iterator it = keys_indices[0].begin();it != keys_indices[0].end();it++) {
        key_0.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
    }
    list<vector<int>*>* matchingTuples_0=auxMap->getValues(key_0);
    if(matchingTuples_0!=NULL && !negativeLiterals[joinOrder[1]] || matchingTuples_0==NULL && negativeLiterals[joinOrder[1]]) {
        if (matchingTuples_0 == NULL) {
            matchingTuples_0 = new list<vector<int>*>(1,NULL);
        }
        for(vector<int>* tuple1: *matchingTuples_0) {
            tupleStack[joinOrder[1]]=tuple1;
            vector<int> head(2);
            head[0] = (*tupleStack[0])[0];
            head[1] = (*tupleStack[1])[1];
            if(wqi!=NULL && !rqi->contains(head)) {
                wqi->insert(head);
            }
            else if(wqi==NULL) {
                rqi->insert(head);
            }
        }
    }
}

void Executor::executeFromFile(const char* filename) {
    DLV2::InputDirector director;
    AspCore2InstanceBuilder* builder = new AspCore2InstanceBuilder();
    director.configureBuilder(builder);
    vector<const char*> fileNames;
    fileNames.push_back(filename);
    director.parse(fileNames);
    executeProgram(builder->getProblemInstance());
    delete builder;
}
void Executor::executeProgram(const vector<Atom> & program) {
    vector<vector<vector<int> > > joinOrders(2);
    joinOrders[0].resize(1);
    joinOrders[0][0].push_back(0);
    joinOrders[1].resize(2);
    joinOrders[1][0].push_back(0);
    joinOrders[1][0].push_back(1);
    joinOrders[1][1].push_back(1);
    joinOrders[1][1].push_back(0);
    map<string, WQi*> wQiMap;
    map<string, RQi*> rQiMap;
    WQi wedge(2, 2);
    wQiMap["edge"] = &wedge;
    WQi wreaches(2, 2);
    wQiMap["reaches"] = &wreaches;
    RQi redge(2, 2);
    rQiMap["edge"] = &redge;
    RQi rreaches(2, 2);
    rQiMap["reaches"] = &rreaches;
    AuxiliaryMap**** auxMaps = new AuxiliaryMap***[2];
    string ruleBodyPredicates_1[] = {"edge", "reaches"};
    vector<bool> negativeLiterals_1;
    vector<int> bodyArieties_1;
    vector<map<int,string> > bodyConstantsOfNegLit_1(2);
    negativeLiterals_1.push_back(false);
    bodyArieties_1.push_back(2);
    negativeLiterals_1.push_back(false);
    bodyArieties_1.push_back(2);
    auxMaps[1]=new AuxiliaryMap**[2];
    auxMaps[1][0]=new AuxiliaryMap*[1];
    auxMaps[1][1]=new AuxiliaryMap*[1];
    vector<vector<AuxiliaryMap*> > auxMapsByAtom_1(2);
    vector<map<int, pair<int,int> > > joinIndices_1_0(1);
    pair<int, int> value_1_0_0_0(0, 1);
    joinIndices_1_0[0][0]=value_1_0_0_0;
    vector<int> keyIndices_1_0_0;
    keyIndices_1_0_0.push_back(0);
    AuxiliaryMap* toAddMap_1_0_0 = new AuxiliaryMap(1, &keyIndices_1_0_0);
    auxMapsByAtom_1[1].push_back(toAddMap_1_0_0);
    auxMaps[1][0][0]= toAddMap_1_0_0;
    vector<map<int, pair<int,int> > > joinIndices_1_1(1);
    pair<int, int> value_1_1_0_1(0, 0);
    joinIndices_1_1[0][1]=value_1_1_0_1;
    vector<int> keyIndices_1_1_0;
    keyIndices_1_1_0.push_back(1);
    AuxiliaryMap* toAddMap_1_1_0 = new AuxiliaryMap(1, &keyIndices_1_1_0);
    auxMapsByAtom_1[0].push_back(toAddMap_1_1_0);
    auxMaps[1][1][0]= toAddMap_1_1_0;
    for(int i=0;i<program.size();i++) {
        if(!wQiMap.count(program[i].getPredicateName())) {
            program[i].print();
            cout<<".\n";
        }
        else {
            vector<int> tuple = program[i].getIntTuple();
            wQiMap[program[i].getPredicateName()]->insert(tuple);
        }
    }
    while(wedge.size() || wreaches.size()) {
        while(wedge.size()) {
            vector<int>* tuple = new vector<int>();
            tuple->reserve(2);
            wedge.next(*tuple);
            wedge.remove(*tuple);
            redge.insert(*tuple);
            vector<int> generatingTuple0(2);
            generatingTuple0[0] = (*tuple)[0];
            generatingTuple0[1] = (*tuple)[1];
            if(!rreaches.contains(generatingTuple0)) {
                wreaches.insert(generatingTuple0);
            }
            performJoin_1(0, &wreaches, &rreaches, joinOrders[1][0], tuple, auxMaps[1][0], joinIndices_1_0, auxMapsByAtom_1[0], negativeLiterals_1);
        }
        while(wreaches.size()) {
            vector<int>* tuple = new vector<int>();
            tuple->reserve(2);
            wreaches.next(*tuple);
            wreaches.remove(*tuple);
            rreaches.insert(*tuple);
            performJoin_1(1, &wreaches, &rreaches, joinOrders[1][1], tuple, auxMaps[1][1], joinIndices_1_1, auxMapsByAtom_1[1], negativeLiterals_1);
        }
    }
    for (map<string, RQi*>::iterator it = rQiMap.begin(); it != rQiMap.end(); it++) {
        it->second->printTuples(it->first);
    }
}
