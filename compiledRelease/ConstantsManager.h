#ifndef CONSTANTSMANAGER_H
#define	CONSTANTSMANAGER_H

#include <unordered_map>
#include <vector>
#include <string>
using namespace std;

class ConstantsManager {
public:
    static ConstantsManager& getInstance() {
        static ConstantsManager instance;
        return instance;
    }
    ConstantsManager();
    int mapConstant(const std::string & constant);
    string unmapConstant(int mapped) const;
private:
    unordered_map<string, int> constantToIntMap;
    vector<string> inverseMap;
    int constantsCounter;
};

#endif	/* CONSTANTSMANAGER_H */

