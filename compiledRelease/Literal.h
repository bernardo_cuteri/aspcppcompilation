#ifndef LITERAL_H
#define	LITERAL_H

#include "Atom.h"

class Literal {
public:
    Literal();
    Literal(bool negated, const Atom & atom);
    void negate();
    bool isNegated() const;
    const string & getTermAt(int i) const;
    bool isVariableTermAt(int i) const;
    const string & getPredicateName() const;
    int getAriety() const;
    const Atom & getAtom() const;
    void print() const;
private:
    bool negated;
    Atom atom;

};

#endif
