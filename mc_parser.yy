%skeleton "lalr1.cc"
%require  "3.0"
%defines 
%define parser_class_name {MC_Parser}

%code requires{
      class AspProgramBuilder;
      class MC_Scanner;
}

%lex-param   { MC_Scanner  &scanner  }
%parse-param { MC_Scanner  &scanner  }

%lex-param   { AspProgramBuilder  *builder  }
%parse-param { AspProgramBuilder  *builder  }

%code{
   #include <iostream>
   #include <cstdlib>
   #include <fstream>
   #include "AspProgramBuilder.h"
  
   static int yylex(yy::MC_Parser::semantic_type *yylval,
                    MC_Scanner  &scanner,
                    AspProgramBuilder   *builder);
   
}

/* token types */
%union {
   std::string *sval;
}

%token            END    0     "end of file"
%token            IMPLICATION
%token            DOT
%token            GTE
%token            LTE
%token            GT
%token            LT
%token            NE
%token   <sval>   WORD
%token            NEWLINE
%token            OB
%token            CB
%token            COMMA
%token            PIPE
%token            NOT
%token            WILD_CARD



/* destructor rule for <sval> objects */
%destructor { std::cout<<"deleting\n"; if ($$)  { delete ($$); ($$) = NULL; } } WORD


%%

program : ruleSet END;

ruleSet : rule | rule ruleSet

rule
	: implicationRule 
	| fact
	;
   

implicationRule
  	: head implicationSign body DOT {builder->addImplicationRule(); builder->dotReached();}
  	;

implicationSign
        : IMPLICATION {builder->implicationSignReached();}
        ;
            

fact
	: head DOT {builder->addFact(); builder->dotReached();}
        ;

head
  	: disjunction 
  	;

disjunction
        : atom
        | disjunction PIPE atom
        ;

body
  	: conjunction
 	;

conjunction
        : conjunct
        | conjunction COMMA conjunct
        ;

conjunct
        : literal
        | expression
        ;


expression
        : inequality
        ;

inequality
        : term inequalitySign term {builder->addInequality();}
        ;

inequalitySign
        : GTE {builder->addInequalitySign(GTE);} 
        | LTE {builder->addInequalitySign(LTE);}
        | GT {builder->addInequalitySign(GT);}
        | LT {builder->addInequalitySign(LT);}
        | NE {builder->addInequalitySign(NE);}
        ;
        


literal 
        : atom
        | NOT atom {builder->negationFound(); }
        ;
atom
        : WORD OB terms CB { builder->addAtom( *$1 ); delete ($1); }
        | WORD { builder->addAtom( *$1 ); delete ($1);}
        ;

terms
        : term 
        | term COMMA terms
        ;

term
        : WORD { builder->addTerm( *$1 ); delete ($1);}
        | WILD_CARD {builder->addTerm("_");}
        ;


%%


void yy::MC_Parser::error( const std::string &err_message )
{
   std::cerr << "Error: " << err_message << "\n"; 
}


/* include for access to scanner.yylex */
#include "mc_scanner.h"
static int yylex( yy::MC_Parser::semantic_type *yylval,
       MC_Scanner  &scanner,
       AspProgramBuilder   *builder )
{
   return( scanner.yylex(yylval) );
}