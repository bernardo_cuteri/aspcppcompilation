#include "Executor.h"

#include "ConstantsManager.h"

#include "DLV2libs/input/InputDirector.h"

#include "AspCore2InstanceBuilder.h"

extern "C" Executor* create_object() {
    return new Executor;
}
extern "C" void destroy_object( Executor* object ) {
    delete object;
}
Executor::~Executor() {}

Executor::Executor() {}

void performJoin_0(unsigned index, WQi* wqi, RQi* rqi, const vector<unsigned> & joinOrder, const vector<unsigned> & tuple0, AuxiliaryMap** auxiliaryMaps, vector<map<unsigned, pair<unsigned, unsigned> > > & keys_indices, vector<AuxiliaryMap* > & atomMaps, const vector<bool> & negativeLiterals, WQi* wQiNeg[]) {
    for(AuxiliaryMap* p: atomMaps) {
        p->insert2(tuple0);
    }
    if(negativeLiterals[index]) {
        return;
    }
    const vector<unsigned>* tupleStack[4];
    tupleStack[index]=&tuple0;
    AuxiliaryMap* auxMap = auxiliaryMaps[0];
    vector<unsigned> key_0;
    for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[0].begin();it != keys_indices[0].end();it++) {
        key_0.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
    }
    if(!negativeLiterals[joinOrder[1]] || !wQiNeg[joinOrder[1]]->contains(key_0)){
        vector<vector<unsigned> >* matchingTuples_0=auxMap->getValues(key_0);
        if((matchingTuples_0!=NULL && !negativeLiterals[joinOrder[1]]) || (matchingTuples_0==NULL && negativeLiterals[joinOrder[1]])) {
            bool negative0 = (matchingTuples_0 == NULL);
            if(negative0) {
                matchingTuples_0 = new vector<vector<unsigned> >(1);
            }
            for(const vector<unsigned> & tuple1: *matchingTuples_0) {
                tupleStack[joinOrder[1]]=&tuple1;
                AuxiliaryMap* auxMap = auxiliaryMaps[1];
                vector<unsigned> key_1;
                for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[1].begin();it != keys_indices[1].end();it++) {
                    key_1.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                }
                if(!negativeLiterals[joinOrder[2]] || !wQiNeg[joinOrder[2]]->contains(key_1)){
                    vector<vector<unsigned> >* matchingTuples_1=auxMap->getValues(key_1);
                    if((matchingTuples_1!=NULL && !negativeLiterals[joinOrder[2]]) || (matchingTuples_1==NULL && negativeLiterals[joinOrder[2]])) {
                        bool negative1 = (matchingTuples_1 == NULL);
                        if(negative1) {
                            matchingTuples_1 = new vector<vector<unsigned> >(1);
                        }
                        for(const vector<unsigned> & tuple2: *matchingTuples_1) {
                            tupleStack[joinOrder[2]]=&tuple2;
                            AuxiliaryMap* auxMap = auxiliaryMaps[2];
                            vector<unsigned> key_2;
                            for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[2].begin();it != keys_indices[2].end();it++) {
                                key_2.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                            }
                            if(!negativeLiterals[joinOrder[3]] || !wQiNeg[joinOrder[3]]->contains(key_2)){
                                vector<vector<unsigned> >* matchingTuples_2=auxMap->getValues(key_2);
                                if((matchingTuples_2!=NULL && !negativeLiterals[joinOrder[3]]) || (matchingTuples_2==NULL && negativeLiterals[joinOrder[3]])) {
                                    bool negative2 = (matchingTuples_2 == NULL);
                                    if(negative2) {
                                        matchingTuples_2 = new vector<vector<unsigned> >(1);
                                    }
                                    for(const vector<unsigned> & tuple3: *matchingTuples_2) {
                                        tupleStack[joinOrder[3]]=&tuple3;
                                        int W1=(*tupleStack[0])[1]+(*tupleStack[1])[1];
                                        int H1=(*tupleStack[0])[2]+(*tupleStack[1])[1];
                                        if((*tupleStack[0])[0]!=(*tupleStack[2])[0] && (*tupleStack[2])[1]>=(*tupleStack[0])[1] && (*tupleStack[2])[1]<W1 && (*tupleStack[2])[2]>=(*tupleStack[0])[2] && (*tupleStack[2])[2]<H1) {
                                            vector<unsigned> head(2);
                                            head[0] = (*tupleStack[1])[0];
                                            head[1] = (*tupleStack[3])[0];
                                            if(wqi!=NULL && !rqi->contains(head)) {
                                                wqi->insert(head);
                                            }
                                            else if(wqi==NULL) {
                                                rqi->insert(head);
                                            }
                                        }
                                    }
                                    if(negative2) {
                                        delete matchingTuples_2;
                                    }
                                }
                            }
                        }
                        if(negative1) {
                            delete matchingTuples_1;
                        }
                    }
                }
            }
            if(negative0) {
                delete matchingTuples_0;
            }
        }
    }
}

void performJoin_1(unsigned index, WQi* wqi, RQi* rqi, const vector<unsigned> & joinOrder, const vector<unsigned> & tuple0, AuxiliaryMap** auxiliaryMaps, vector<map<unsigned, pair<unsigned, unsigned> > > & keys_indices, vector<AuxiliaryMap* > & atomMaps, const vector<bool> & negativeLiterals, WQi* wQiNeg[]) {
    for(AuxiliaryMap* p: atomMaps) {
        p->insert2(tuple0);
    }
    if(negativeLiterals[index]) {
        return;
    }
    const vector<unsigned>* tupleStack[4];
    tupleStack[index]=&tuple0;
    AuxiliaryMap* auxMap = auxiliaryMaps[0];
    vector<unsigned> key_0;
    for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[0].begin();it != keys_indices[0].end();it++) {
        key_0.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
    }
    if(!negativeLiterals[joinOrder[1]] || !wQiNeg[joinOrder[1]]->contains(key_0)){
        vector<vector<unsigned> >* matchingTuples_0=auxMap->getValues(key_0);
        if((matchingTuples_0!=NULL && !negativeLiterals[joinOrder[1]]) || (matchingTuples_0==NULL && negativeLiterals[joinOrder[1]])) {
            bool negative0 = (matchingTuples_0 == NULL);
            if(negative0) {
                matchingTuples_0 = new vector<vector<unsigned> >(1);
            }
            for(const vector<unsigned> & tuple1: *matchingTuples_0) {
                tupleStack[joinOrder[1]]=&tuple1;
                AuxiliaryMap* auxMap = auxiliaryMaps[1];
                vector<unsigned> key_1;
                for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[1].begin();it != keys_indices[1].end();it++) {
                    key_1.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                }
                if(!negativeLiterals[joinOrder[2]] || !wQiNeg[joinOrder[2]]->contains(key_1)){
                    vector<vector<unsigned> >* matchingTuples_1=auxMap->getValues(key_1);
                    if((matchingTuples_1!=NULL && !negativeLiterals[joinOrder[2]]) || (matchingTuples_1==NULL && negativeLiterals[joinOrder[2]])) {
                        bool negative1 = (matchingTuples_1 == NULL);
                        if(negative1) {
                            matchingTuples_1 = new vector<vector<unsigned> >(1);
                        }
                        for(const vector<unsigned> & tuple2: *matchingTuples_1) {
                            tupleStack[joinOrder[2]]=&tuple2;
                            AuxiliaryMap* auxMap = auxiliaryMaps[2];
                            vector<unsigned> key_2;
                            for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[2].begin();it != keys_indices[2].end();it++) {
                                key_2.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                            }
                            if(!negativeLiterals[joinOrder[3]] || !wQiNeg[joinOrder[3]]->contains(key_2)){
                                vector<vector<unsigned> >* matchingTuples_2=auxMap->getValues(key_2);
                                if((matchingTuples_2!=NULL && !negativeLiterals[joinOrder[3]]) || (matchingTuples_2==NULL && negativeLiterals[joinOrder[3]])) {
                                    bool negative2 = (matchingTuples_2 == NULL);
                                    if(negative2) {
                                        matchingTuples_2 = new vector<vector<unsigned> >(1);
                                    }
                                    for(const vector<unsigned> & tuple3: *matchingTuples_2) {
                                        tupleStack[joinOrder[3]]=&tuple3;
                                        int W1=(*tupleStack[0])[1]+(*tupleStack[1])[1];
                                        int H1=(*tupleStack[0])[2]+(*tupleStack[1])[1];
                                        int W2=(*tupleStack[2])[1]+(*tupleStack[3])[1];
                                        if((*tupleStack[0])[0]!=(*tupleStack[2])[0] && W2>(*tupleStack[0])[1] && W2<=W1 && (*tupleStack[2])[2]>=(*tupleStack[0])[2] && (*tupleStack[2])[2]<H1) {
                                            vector<unsigned> head(2);
                                            head[0] = (*tupleStack[1])[0];
                                            head[1] = (*tupleStack[3])[0];
                                            if(wqi!=NULL && !rqi->contains(head)) {
                                                wqi->insert(head);
                                            }
                                            else if(wqi==NULL) {
                                                rqi->insert(head);
                                            }
                                        }
                                    }
                                    if(negative2) {
                                        delete matchingTuples_2;
                                    }
                                }
                            }
                        }
                        if(negative1) {
                            delete matchingTuples_1;
                        }
                    }
                }
            }
            if(negative0) {
                delete matchingTuples_0;
            }
        }
    }
}

void performJoin_2(unsigned index, WQi* wqi, RQi* rqi, const vector<unsigned> & joinOrder, const vector<unsigned> & tuple0, AuxiliaryMap** auxiliaryMaps, vector<map<unsigned, pair<unsigned, unsigned> > > & keys_indices, vector<AuxiliaryMap* > & atomMaps, const vector<bool> & negativeLiterals, WQi* wQiNeg[]) {
    for(AuxiliaryMap* p: atomMaps) {
        p->insert2(tuple0);
    }
    if(negativeLiterals[index]) {
        return;
    }
    const vector<unsigned>* tupleStack[4];
    tupleStack[index]=&tuple0;
    AuxiliaryMap* auxMap = auxiliaryMaps[0];
    vector<unsigned> key_0;
    for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[0].begin();it != keys_indices[0].end();it++) {
        key_0.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
    }
    if(!negativeLiterals[joinOrder[1]] || !wQiNeg[joinOrder[1]]->contains(key_0)){
        vector<vector<unsigned> >* matchingTuples_0=auxMap->getValues(key_0);
        if((matchingTuples_0!=NULL && !negativeLiterals[joinOrder[1]]) || (matchingTuples_0==NULL && negativeLiterals[joinOrder[1]])) {
            bool negative0 = (matchingTuples_0 == NULL);
            if(negative0) {
                matchingTuples_0 = new vector<vector<unsigned> >(1);
            }
            for(const vector<unsigned> & tuple1: *matchingTuples_0) {
                tupleStack[joinOrder[1]]=&tuple1;
                AuxiliaryMap* auxMap = auxiliaryMaps[1];
                vector<unsigned> key_1;
                for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[1].begin();it != keys_indices[1].end();it++) {
                    key_1.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                }
                if(!negativeLiterals[joinOrder[2]] || !wQiNeg[joinOrder[2]]->contains(key_1)){
                    vector<vector<unsigned> >* matchingTuples_1=auxMap->getValues(key_1);
                    if((matchingTuples_1!=NULL && !negativeLiterals[joinOrder[2]]) || (matchingTuples_1==NULL && negativeLiterals[joinOrder[2]])) {
                        bool negative1 = (matchingTuples_1 == NULL);
                        if(negative1) {
                            matchingTuples_1 = new vector<vector<unsigned> >(1);
                        }
                        for(const vector<unsigned> & tuple2: *matchingTuples_1) {
                            tupleStack[joinOrder[2]]=&tuple2;
                            AuxiliaryMap* auxMap = auxiliaryMaps[2];
                            vector<unsigned> key_2;
                            for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[2].begin();it != keys_indices[2].end();it++) {
                                key_2.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                            }
                            if(!negativeLiterals[joinOrder[3]] || !wQiNeg[joinOrder[3]]->contains(key_2)){
                                vector<vector<unsigned> >* matchingTuples_2=auxMap->getValues(key_2);
                                if((matchingTuples_2!=NULL && !negativeLiterals[joinOrder[3]]) || (matchingTuples_2==NULL && negativeLiterals[joinOrder[3]])) {
                                    bool negative2 = (matchingTuples_2 == NULL);
                                    if(negative2) {
                                        matchingTuples_2 = new vector<vector<unsigned> >(1);
                                    }
                                    for(const vector<unsigned> & tuple3: *matchingTuples_2) {
                                        tupleStack[joinOrder[3]]=&tuple3;
                                        int W1=(*tupleStack[0])[1]+(*tupleStack[1])[1];
                                        int H1=(*tupleStack[0])[2]+(*tupleStack[1])[1];
                                        int H2=(*tupleStack[2])[2]+(*tupleStack[3])[1];
                                        if((*tupleStack[0])[0]!=(*tupleStack[2])[0] && (*tupleStack[2])[1]>=(*tupleStack[0])[1] && (*tupleStack[2])[1]<W1 && H2>(*tupleStack[0])[2] && H2<=H1) {
                                            vector<unsigned> head(2);
                                            head[0] = (*tupleStack[1])[0];
                                            head[1] = (*tupleStack[3])[0];
                                            if(wqi!=NULL && !rqi->contains(head)) {
                                                wqi->insert(head);
                                            }
                                            else if(wqi==NULL) {
                                                rqi->insert(head);
                                            }
                                        }
                                    }
                                    if(negative2) {
                                        delete matchingTuples_2;
                                    }
                                }
                            }
                        }
                        if(negative1) {
                            delete matchingTuples_1;
                        }
                    }
                }
            }
            if(negative0) {
                delete matchingTuples_0;
            }
        }
    }
}

void performJoin_3(unsigned index, WQi* wqi, RQi* rqi, const vector<unsigned> & joinOrder, const vector<unsigned> & tuple0, AuxiliaryMap** auxiliaryMaps, vector<map<unsigned, pair<unsigned, unsigned> > > & keys_indices, vector<AuxiliaryMap* > & atomMaps, const vector<bool> & negativeLiterals, WQi* wQiNeg[]) {
    for(AuxiliaryMap* p: atomMaps) {
        p->insert2(tuple0);
    }
    if(negativeLiterals[index]) {
        return;
    }
    const vector<unsigned>* tupleStack[4];
    tupleStack[index]=&tuple0;
    AuxiliaryMap* auxMap = auxiliaryMaps[0];
    vector<unsigned> key_0;
    for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[0].begin();it != keys_indices[0].end();it++) {
        key_0.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
    }
    if(!negativeLiterals[joinOrder[1]] || !wQiNeg[joinOrder[1]]->contains(key_0)){
        vector<vector<unsigned> >* matchingTuples_0=auxMap->getValues(key_0);
        if((matchingTuples_0!=NULL && !negativeLiterals[joinOrder[1]]) || (matchingTuples_0==NULL && negativeLiterals[joinOrder[1]])) {
            bool negative0 = (matchingTuples_0 == NULL);
            if(negative0) {
                matchingTuples_0 = new vector<vector<unsigned> >(1);
            }
            for(const vector<unsigned> & tuple1: *matchingTuples_0) {
                tupleStack[joinOrder[1]]=&tuple1;
                AuxiliaryMap* auxMap = auxiliaryMaps[1];
                vector<unsigned> key_1;
                for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[1].begin();it != keys_indices[1].end();it++) {
                    key_1.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                }
                if(!negativeLiterals[joinOrder[2]] || !wQiNeg[joinOrder[2]]->contains(key_1)){
                    vector<vector<unsigned> >* matchingTuples_1=auxMap->getValues(key_1);
                    if((matchingTuples_1!=NULL && !negativeLiterals[joinOrder[2]]) || (matchingTuples_1==NULL && negativeLiterals[joinOrder[2]])) {
                        bool negative1 = (matchingTuples_1 == NULL);
                        if(negative1) {
                            matchingTuples_1 = new vector<vector<unsigned> >(1);
                        }
                        for(const vector<unsigned> & tuple2: *matchingTuples_1) {
                            tupleStack[joinOrder[2]]=&tuple2;
                            AuxiliaryMap* auxMap = auxiliaryMaps[2];
                            vector<unsigned> key_2;
                            for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[2].begin();it != keys_indices[2].end();it++) {
                                key_2.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                            }
                            if(!negativeLiterals[joinOrder[3]] || !wQiNeg[joinOrder[3]]->contains(key_2)){
                                vector<vector<unsigned> >* matchingTuples_2=auxMap->getValues(key_2);
                                if((matchingTuples_2!=NULL && !negativeLiterals[joinOrder[3]]) || (matchingTuples_2==NULL && negativeLiterals[joinOrder[3]])) {
                                    bool negative2 = (matchingTuples_2 == NULL);
                                    if(negative2) {
                                        matchingTuples_2 = new vector<vector<unsigned> >(1);
                                    }
                                    for(const vector<unsigned> & tuple3: *matchingTuples_2) {
                                        tupleStack[joinOrder[3]]=&tuple3;
                                        int W1=(*tupleStack[0])[1]+(*tupleStack[1])[1];
                                        int H1=(*tupleStack[0])[2]+(*tupleStack[1])[1];
                                        int W2=(*tupleStack[2])[1]+(*tupleStack[3])[1];
                                        int H2=(*tupleStack[2])[2]+(*tupleStack[3])[1];
                                        if((*tupleStack[0])[0]!=(*tupleStack[2])[0] && W2>(*tupleStack[0])[1] && W2<=W1 && H2>(*tupleStack[0])[2] && H2<=H1) {
                                            vector<unsigned> head(2);
                                            head[0] = (*tupleStack[1])[0];
                                            head[1] = (*tupleStack[3])[0];
                                            if(wqi!=NULL && !rqi->contains(head)) {
                                                wqi->insert(head);
                                            }
                                            else if(wqi==NULL) {
                                                rqi->insert(head);
                                            }
                                        }
                                    }
                                    if(negative2) {
                                        delete matchingTuples_2;
                                    }
                                }
                            }
                        }
                        if(negative1) {
                            delete matchingTuples_1;
                        }
                    }
                }
            }
            if(negative0) {
                delete matchingTuples_0;
            }
        }
    }
}

void Executor::executeFromFile(const char* filename) {
    DLV2::InputDirector director;
    AspCore2InstanceBuilder* builder = new AspCore2InstanceBuilder();
    director.configureBuilder(builder);
    vector<const char*> fileNames;
    fileNames.push_back(filename);
    director.parse(fileNames);
    executeProgramOnFacts(builder->getProblemInstance());
    delete builder;
}
void Executor::executeProgramOnFacts(const vector<aspc::Atom> & program) {
    failedConstraints.erase(failedConstraints.begin(), failedConstraints.end());
    vector<vector<vector<unsigned> > > joinOrders(4);
    joinOrders[0].resize(4);
    joinOrders[0][0].push_back(0);
    joinOrders[0][0].push_back(3);
    joinOrders[0][0].push_back(2);
    joinOrders[0][0].push_back(1);
    joinOrders[0][1].push_back(1);
    joinOrders[0][1].push_back(3);
    joinOrders[0][1].push_back(2);
    joinOrders[0][1].push_back(0);
    joinOrders[0][2].push_back(2);
    joinOrders[0][2].push_back(3);
    joinOrders[0][2].push_back(1);
    joinOrders[0][2].push_back(0);
    joinOrders[0][3].push_back(3);
    joinOrders[0][3].push_back(2);
    joinOrders[0][3].push_back(1);
    joinOrders[0][3].push_back(0);
    joinOrders[1].resize(4);
    joinOrders[1][0].push_back(0);
    joinOrders[1][0].push_back(3);
    joinOrders[1][0].push_back(2);
    joinOrders[1][0].push_back(1);
    joinOrders[1][1].push_back(1);
    joinOrders[1][1].push_back(3);
    joinOrders[1][1].push_back(2);
    joinOrders[1][1].push_back(0);
    joinOrders[1][2].push_back(2);
    joinOrders[1][2].push_back(3);
    joinOrders[1][2].push_back(1);
    joinOrders[1][2].push_back(0);
    joinOrders[1][3].push_back(3);
    joinOrders[1][3].push_back(2);
    joinOrders[1][3].push_back(1);
    joinOrders[1][3].push_back(0);
    joinOrders[2].resize(4);
    joinOrders[2][0].push_back(0);
    joinOrders[2][0].push_back(3);
    joinOrders[2][0].push_back(2);
    joinOrders[2][0].push_back(1);
    joinOrders[2][1].push_back(1);
    joinOrders[2][1].push_back(3);
    joinOrders[2][1].push_back(2);
    joinOrders[2][1].push_back(0);
    joinOrders[2][2].push_back(2);
    joinOrders[2][2].push_back(3);
    joinOrders[2][2].push_back(1);
    joinOrders[2][2].push_back(0);
    joinOrders[2][3].push_back(3);
    joinOrders[2][3].push_back(2);
    joinOrders[2][3].push_back(1);
    joinOrders[2][3].push_back(0);
    joinOrders[3].resize(4);
    joinOrders[3][0].push_back(0);
    joinOrders[3][0].push_back(3);
    joinOrders[3][0].push_back(2);
    joinOrders[3][0].push_back(1);
    joinOrders[3][1].push_back(1);
    joinOrders[3][1].push_back(3);
    joinOrders[3][1].push_back(2);
    joinOrders[3][1].push_back(0);
    joinOrders[3][2].push_back(2);
    joinOrders[3][2].push_back(3);
    joinOrders[3][2].push_back(1);
    joinOrders[3][2].push_back(0);
    joinOrders[3][3].push_back(3);
    joinOrders[3][3].push_back(2);
    joinOrders[3][3].push_back(1);
    joinOrders[3][3].push_back(0);
    map<string, WQi*> wQiMap;
    map<string, RQi*> rQiMap;
    WQi wpos(3, 3);
    wQiMap["pos"] = &wpos;
    WQi wsquare(2, 2);
    wQiMap["square"] = &wsquare;
    RQi roverl(2, 2);
    rQiMap["overl"] = &roverl;
    RQi rpos(3, 3);
    rQiMap["pos"] = &rpos;
    RQi rsquare(2, 2);
    rQiMap["square"] = &rsquare;
    string ruleBodyPredicates_0[] = {"pos", "square", "pos", "square"};
    vector<bool> negativeLiterals_0;
    vector<unsigned> bodyArieties_0;
    vector<map<unsigned,string> > bodyConstantsOfNegLit_0(4);
    negativeLiterals_0.push_back(false);
    bodyArieties_0.push_back(3);
    negativeLiterals_0.push_back(false);
    bodyArieties_0.push_back(2);
    negativeLiterals_0.push_back(false);
    bodyArieties_0.push_back(3);
    negativeLiterals_0.push_back(false);
    bodyArieties_0.push_back(2);
    AuxiliaryMap* auxMaps_0_0[3];
    AuxiliaryMap* auxMaps_0_1[3];
    AuxiliaryMap* auxMaps_0_2[3];
    AuxiliaryMap* auxMaps_0_3[3];
    vector<vector<AuxiliaryMap*> > auxMapsByAtom_0(4);
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_0_0(3);
    vector<unsigned> keyIndices_0_0_0;
    AuxiliaryMap toAddMap_0_0_0(0, &keyIndices_0_0_0);
    auxMapsByAtom_0[3].push_back(&toAddMap_0_0_0);
    auxMaps_0_0[0]= &toAddMap_0_0_0;
    pair<unsigned, unsigned> value_0_0_1_0(1, 0);
    joinIndices_0_0[1][0]=value_0_0_1_0;
    vector<unsigned> keyIndices_0_0_1;
    keyIndices_0_0_1.push_back(0);
    AuxiliaryMap toAddMap_0_0_1(1, &keyIndices_0_0_1);
    auxMapsByAtom_0[2].push_back(&toAddMap_0_0_1);
    auxMaps_0_0[1]= &toAddMap_0_0_1;
    pair<unsigned, unsigned> value_0_0_2_0(0, 0);
    joinIndices_0_0[2][0]=value_0_0_2_0;
    vector<unsigned> keyIndices_0_0_2;
    keyIndices_0_0_2.push_back(0);
    AuxiliaryMap toAddMap_0_0_2(1, &keyIndices_0_0_2);
    auxMapsByAtom_0[1].push_back(&toAddMap_0_0_2);
    auxMaps_0_0[2]= &toAddMap_0_0_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_0_1(3);
    vector<unsigned> keyIndices_0_1_0;
    AuxiliaryMap toAddMap_0_1_0(0, &keyIndices_0_1_0);
    auxMapsByAtom_0[3].push_back(&toAddMap_0_1_0);
    auxMaps_0_1[0]= &toAddMap_0_1_0;
    pair<unsigned, unsigned> value_0_1_1_0(1, 0);
    joinIndices_0_1[1][0]=value_0_1_1_0;
    vector<unsigned> keyIndices_0_1_1;
    keyIndices_0_1_1.push_back(0);
    AuxiliaryMap toAddMap_0_1_1(1, &keyIndices_0_1_1);
    auxMapsByAtom_0[2].push_back(&toAddMap_0_1_1);
    auxMaps_0_1[1]= &toAddMap_0_1_1;
    pair<unsigned, unsigned> value_0_1_2_0(0, 0);
    joinIndices_0_1[2][0]=value_0_1_2_0;
    vector<unsigned> keyIndices_0_1_2;
    keyIndices_0_1_2.push_back(0);
    AuxiliaryMap toAddMap_0_1_2(1, &keyIndices_0_1_2);
    auxMapsByAtom_0[0].push_back(&toAddMap_0_1_2);
    auxMaps_0_1[2]= &toAddMap_0_1_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_0_2(3);
    pair<unsigned, unsigned> value_0_2_0_0(0, 0);
    joinIndices_0_2[0][0]=value_0_2_0_0;
    vector<unsigned> keyIndices_0_2_0;
    keyIndices_0_2_0.push_back(0);
    AuxiliaryMap toAddMap_0_2_0(1, &keyIndices_0_2_0);
    auxMapsByAtom_0[3].push_back(&toAddMap_0_2_0);
    auxMaps_0_2[0]= &toAddMap_0_2_0;
    vector<unsigned> keyIndices_0_2_1;
    AuxiliaryMap toAddMap_0_2_1(0, &keyIndices_0_2_1);
    auxMapsByAtom_0[1].push_back(&toAddMap_0_2_1);
    auxMaps_0_2[1]= &toAddMap_0_2_1;
    pair<unsigned, unsigned> value_0_2_2_0(2, 0);
    joinIndices_0_2[2][0]=value_0_2_2_0;
    vector<unsigned> keyIndices_0_2_2;
    keyIndices_0_2_2.push_back(0);
    AuxiliaryMap toAddMap_0_2_2(1, &keyIndices_0_2_2);
    auxMapsByAtom_0[0].push_back(&toAddMap_0_2_2);
    auxMaps_0_2[2]= &toAddMap_0_2_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_0_3(3);
    pair<unsigned, unsigned> value_0_3_0_0(0, 0);
    joinIndices_0_3[0][0]=value_0_3_0_0;
    vector<unsigned> keyIndices_0_3_0;
    keyIndices_0_3_0.push_back(0);
    AuxiliaryMap toAddMap_0_3_0(1, &keyIndices_0_3_0);
    auxMapsByAtom_0[2].push_back(&toAddMap_0_3_0);
    auxMaps_0_3[0]= &toAddMap_0_3_0;
    vector<unsigned> keyIndices_0_3_1;
    AuxiliaryMap toAddMap_0_3_1(0, &keyIndices_0_3_1);
    auxMapsByAtom_0[1].push_back(&toAddMap_0_3_1);
    auxMaps_0_3[1]= &toAddMap_0_3_1;
    pair<unsigned, unsigned> value_0_3_2_0(2, 0);
    joinIndices_0_3[2][0]=value_0_3_2_0;
    vector<unsigned> keyIndices_0_3_2;
    keyIndices_0_3_2.push_back(0);
    AuxiliaryMap toAddMap_0_3_2(1, &keyIndices_0_3_2);
    auxMapsByAtom_0[0].push_back(&toAddMap_0_3_2);
    auxMaps_0_3[2]= &toAddMap_0_3_2;
    string ruleBodyPredicates_1[] = {"pos", "square", "pos", "square"};
    vector<bool> negativeLiterals_1;
    vector<unsigned> bodyArieties_1;
    vector<map<unsigned,string> > bodyConstantsOfNegLit_1(4);
    negativeLiterals_1.push_back(false);
    bodyArieties_1.push_back(3);
    negativeLiterals_1.push_back(false);
    bodyArieties_1.push_back(2);
    negativeLiterals_1.push_back(false);
    bodyArieties_1.push_back(3);
    negativeLiterals_1.push_back(false);
    bodyArieties_1.push_back(2);
    AuxiliaryMap* auxMaps_1_0[3];
    AuxiliaryMap* auxMaps_1_1[3];
    AuxiliaryMap* auxMaps_1_2[3];
    AuxiliaryMap* auxMaps_1_3[3];
    vector<vector<AuxiliaryMap*> > auxMapsByAtom_1(4);
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_1_0(3);
    vector<unsigned> keyIndices_1_0_0;
    AuxiliaryMap toAddMap_1_0_0(0, &keyIndices_1_0_0);
    auxMapsByAtom_1[3].push_back(&toAddMap_1_0_0);
    auxMaps_1_0[0]= &toAddMap_1_0_0;
    pair<unsigned, unsigned> value_1_0_1_0(1, 0);
    joinIndices_1_0[1][0]=value_1_0_1_0;
    vector<unsigned> keyIndices_1_0_1;
    keyIndices_1_0_1.push_back(0);
    AuxiliaryMap toAddMap_1_0_1(1, &keyIndices_1_0_1);
    auxMapsByAtom_1[2].push_back(&toAddMap_1_0_1);
    auxMaps_1_0[1]= &toAddMap_1_0_1;
    pair<unsigned, unsigned> value_1_0_2_0(0, 0);
    joinIndices_1_0[2][0]=value_1_0_2_0;
    vector<unsigned> keyIndices_1_0_2;
    keyIndices_1_0_2.push_back(0);
    AuxiliaryMap toAddMap_1_0_2(1, &keyIndices_1_0_2);
    auxMapsByAtom_1[1].push_back(&toAddMap_1_0_2);
    auxMaps_1_0[2]= &toAddMap_1_0_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_1_1(3);
    vector<unsigned> keyIndices_1_1_0;
    AuxiliaryMap toAddMap_1_1_0(0, &keyIndices_1_1_0);
    auxMapsByAtom_1[3].push_back(&toAddMap_1_1_0);
    auxMaps_1_1[0]= &toAddMap_1_1_0;
    pair<unsigned, unsigned> value_1_1_1_0(1, 0);
    joinIndices_1_1[1][0]=value_1_1_1_0;
    vector<unsigned> keyIndices_1_1_1;
    keyIndices_1_1_1.push_back(0);
    AuxiliaryMap toAddMap_1_1_1(1, &keyIndices_1_1_1);
    auxMapsByAtom_1[2].push_back(&toAddMap_1_1_1);
    auxMaps_1_1[1]= &toAddMap_1_1_1;
    pair<unsigned, unsigned> value_1_1_2_0(0, 0);
    joinIndices_1_1[2][0]=value_1_1_2_0;
    vector<unsigned> keyIndices_1_1_2;
    keyIndices_1_1_2.push_back(0);
    AuxiliaryMap toAddMap_1_1_2(1, &keyIndices_1_1_2);
    auxMapsByAtom_1[0].push_back(&toAddMap_1_1_2);
    auxMaps_1_1[2]= &toAddMap_1_1_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_1_2(3);
    pair<unsigned, unsigned> value_1_2_0_0(0, 0);
    joinIndices_1_2[0][0]=value_1_2_0_0;
    vector<unsigned> keyIndices_1_2_0;
    keyIndices_1_2_0.push_back(0);
    AuxiliaryMap toAddMap_1_2_0(1, &keyIndices_1_2_0);
    auxMapsByAtom_1[3].push_back(&toAddMap_1_2_0);
    auxMaps_1_2[0]= &toAddMap_1_2_0;
    vector<unsigned> keyIndices_1_2_1;
    AuxiliaryMap toAddMap_1_2_1(0, &keyIndices_1_2_1);
    auxMapsByAtom_1[1].push_back(&toAddMap_1_2_1);
    auxMaps_1_2[1]= &toAddMap_1_2_1;
    pair<unsigned, unsigned> value_1_2_2_0(2, 0);
    joinIndices_1_2[2][0]=value_1_2_2_0;
    vector<unsigned> keyIndices_1_2_2;
    keyIndices_1_2_2.push_back(0);
    AuxiliaryMap toAddMap_1_2_2(1, &keyIndices_1_2_2);
    auxMapsByAtom_1[0].push_back(&toAddMap_1_2_2);
    auxMaps_1_2[2]= &toAddMap_1_2_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_1_3(3);
    pair<unsigned, unsigned> value_1_3_0_0(0, 0);
    joinIndices_1_3[0][0]=value_1_3_0_0;
    vector<unsigned> keyIndices_1_3_0;
    keyIndices_1_3_0.push_back(0);
    AuxiliaryMap toAddMap_1_3_0(1, &keyIndices_1_3_0);
    auxMapsByAtom_1[2].push_back(&toAddMap_1_3_0);
    auxMaps_1_3[0]= &toAddMap_1_3_0;
    vector<unsigned> keyIndices_1_3_1;
    AuxiliaryMap toAddMap_1_3_1(0, &keyIndices_1_3_1);
    auxMapsByAtom_1[1].push_back(&toAddMap_1_3_1);
    auxMaps_1_3[1]= &toAddMap_1_3_1;
    pair<unsigned, unsigned> value_1_3_2_0(2, 0);
    joinIndices_1_3[2][0]=value_1_3_2_0;
    vector<unsigned> keyIndices_1_3_2;
    keyIndices_1_3_2.push_back(0);
    AuxiliaryMap toAddMap_1_3_2(1, &keyIndices_1_3_2);
    auxMapsByAtom_1[0].push_back(&toAddMap_1_3_2);
    auxMaps_1_3[2]= &toAddMap_1_3_2;
    string ruleBodyPredicates_2[] = {"pos", "square", "pos", "square"};
    vector<bool> negativeLiterals_2;
    vector<unsigned> bodyArieties_2;
    vector<map<unsigned,string> > bodyConstantsOfNegLit_2(4);
    negativeLiterals_2.push_back(false);
    bodyArieties_2.push_back(3);
    negativeLiterals_2.push_back(false);
    bodyArieties_2.push_back(2);
    negativeLiterals_2.push_back(false);
    bodyArieties_2.push_back(3);
    negativeLiterals_2.push_back(false);
    bodyArieties_2.push_back(2);
    AuxiliaryMap* auxMaps_2_0[3];
    AuxiliaryMap* auxMaps_2_1[3];
    AuxiliaryMap* auxMaps_2_2[3];
    AuxiliaryMap* auxMaps_2_3[3];
    vector<vector<AuxiliaryMap*> > auxMapsByAtom_2(4);
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_2_0(3);
    vector<unsigned> keyIndices_2_0_0;
    AuxiliaryMap toAddMap_2_0_0(0, &keyIndices_2_0_0);
    auxMapsByAtom_2[3].push_back(&toAddMap_2_0_0);
    auxMaps_2_0[0]= &toAddMap_2_0_0;
    pair<unsigned, unsigned> value_2_0_1_0(1, 0);
    joinIndices_2_0[1][0]=value_2_0_1_0;
    vector<unsigned> keyIndices_2_0_1;
    keyIndices_2_0_1.push_back(0);
    AuxiliaryMap toAddMap_2_0_1(1, &keyIndices_2_0_1);
    auxMapsByAtom_2[2].push_back(&toAddMap_2_0_1);
    auxMaps_2_0[1]= &toAddMap_2_0_1;
    pair<unsigned, unsigned> value_2_0_2_0(0, 0);
    joinIndices_2_0[2][0]=value_2_0_2_0;
    vector<unsigned> keyIndices_2_0_2;
    keyIndices_2_0_2.push_back(0);
    AuxiliaryMap toAddMap_2_0_2(1, &keyIndices_2_0_2);
    auxMapsByAtom_2[1].push_back(&toAddMap_2_0_2);
    auxMaps_2_0[2]= &toAddMap_2_0_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_2_1(3);
    vector<unsigned> keyIndices_2_1_0;
    AuxiliaryMap toAddMap_2_1_0(0, &keyIndices_2_1_0);
    auxMapsByAtom_2[3].push_back(&toAddMap_2_1_0);
    auxMaps_2_1[0]= &toAddMap_2_1_0;
    pair<unsigned, unsigned> value_2_1_1_0(1, 0);
    joinIndices_2_1[1][0]=value_2_1_1_0;
    vector<unsigned> keyIndices_2_1_1;
    keyIndices_2_1_1.push_back(0);
    AuxiliaryMap toAddMap_2_1_1(1, &keyIndices_2_1_1);
    auxMapsByAtom_2[2].push_back(&toAddMap_2_1_1);
    auxMaps_2_1[1]= &toAddMap_2_1_1;
    pair<unsigned, unsigned> value_2_1_2_0(0, 0);
    joinIndices_2_1[2][0]=value_2_1_2_0;
    vector<unsigned> keyIndices_2_1_2;
    keyIndices_2_1_2.push_back(0);
    AuxiliaryMap toAddMap_2_1_2(1, &keyIndices_2_1_2);
    auxMapsByAtom_2[0].push_back(&toAddMap_2_1_2);
    auxMaps_2_1[2]= &toAddMap_2_1_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_2_2(3);
    pair<unsigned, unsigned> value_2_2_0_0(0, 0);
    joinIndices_2_2[0][0]=value_2_2_0_0;
    vector<unsigned> keyIndices_2_2_0;
    keyIndices_2_2_0.push_back(0);
    AuxiliaryMap toAddMap_2_2_0(1, &keyIndices_2_2_0);
    auxMapsByAtom_2[3].push_back(&toAddMap_2_2_0);
    auxMaps_2_2[0]= &toAddMap_2_2_0;
    vector<unsigned> keyIndices_2_2_1;
    AuxiliaryMap toAddMap_2_2_1(0, &keyIndices_2_2_1);
    auxMapsByAtom_2[1].push_back(&toAddMap_2_2_1);
    auxMaps_2_2[1]= &toAddMap_2_2_1;
    pair<unsigned, unsigned> value_2_2_2_0(2, 0);
    joinIndices_2_2[2][0]=value_2_2_2_0;
    vector<unsigned> keyIndices_2_2_2;
    keyIndices_2_2_2.push_back(0);
    AuxiliaryMap toAddMap_2_2_2(1, &keyIndices_2_2_2);
    auxMapsByAtom_2[0].push_back(&toAddMap_2_2_2);
    auxMaps_2_2[2]= &toAddMap_2_2_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_2_3(3);
    pair<unsigned, unsigned> value_2_3_0_0(0, 0);
    joinIndices_2_3[0][0]=value_2_3_0_0;
    vector<unsigned> keyIndices_2_3_0;
    keyIndices_2_3_0.push_back(0);
    AuxiliaryMap toAddMap_2_3_0(1, &keyIndices_2_3_0);
    auxMapsByAtom_2[2].push_back(&toAddMap_2_3_0);
    auxMaps_2_3[0]= &toAddMap_2_3_0;
    vector<unsigned> keyIndices_2_3_1;
    AuxiliaryMap toAddMap_2_3_1(0, &keyIndices_2_3_1);
    auxMapsByAtom_2[1].push_back(&toAddMap_2_3_1);
    auxMaps_2_3[1]= &toAddMap_2_3_1;
    pair<unsigned, unsigned> value_2_3_2_0(2, 0);
    joinIndices_2_3[2][0]=value_2_3_2_0;
    vector<unsigned> keyIndices_2_3_2;
    keyIndices_2_3_2.push_back(0);
    AuxiliaryMap toAddMap_2_3_2(1, &keyIndices_2_3_2);
    auxMapsByAtom_2[0].push_back(&toAddMap_2_3_2);
    auxMaps_2_3[2]= &toAddMap_2_3_2;
    string ruleBodyPredicates_3[] = {"pos", "square", "pos", "square"};
    vector<bool> negativeLiterals_3;
    vector<unsigned> bodyArieties_3;
    vector<map<unsigned,string> > bodyConstantsOfNegLit_3(4);
    negativeLiterals_3.push_back(false);
    bodyArieties_3.push_back(3);
    negativeLiterals_3.push_back(false);
    bodyArieties_3.push_back(2);
    negativeLiterals_3.push_back(false);
    bodyArieties_3.push_back(3);
    negativeLiterals_3.push_back(false);
    bodyArieties_3.push_back(2);
    AuxiliaryMap* auxMaps_3_0[3];
    AuxiliaryMap* auxMaps_3_1[3];
    AuxiliaryMap* auxMaps_3_2[3];
    AuxiliaryMap* auxMaps_3_3[3];
    vector<vector<AuxiliaryMap*> > auxMapsByAtom_3(4);
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_3_0(3);
    vector<unsigned> keyIndices_3_0_0;
    AuxiliaryMap toAddMap_3_0_0(0, &keyIndices_3_0_0);
    auxMapsByAtom_3[3].push_back(&toAddMap_3_0_0);
    auxMaps_3_0[0]= &toAddMap_3_0_0;
    pair<unsigned, unsigned> value_3_0_1_0(1, 0);
    joinIndices_3_0[1][0]=value_3_0_1_0;
    vector<unsigned> keyIndices_3_0_1;
    keyIndices_3_0_1.push_back(0);
    AuxiliaryMap toAddMap_3_0_1(1, &keyIndices_3_0_1);
    auxMapsByAtom_3[2].push_back(&toAddMap_3_0_1);
    auxMaps_3_0[1]= &toAddMap_3_0_1;
    pair<unsigned, unsigned> value_3_0_2_0(0, 0);
    joinIndices_3_0[2][0]=value_3_0_2_0;
    vector<unsigned> keyIndices_3_0_2;
    keyIndices_3_0_2.push_back(0);
    AuxiliaryMap toAddMap_3_0_2(1, &keyIndices_3_0_2);
    auxMapsByAtom_3[1].push_back(&toAddMap_3_0_2);
    auxMaps_3_0[2]= &toAddMap_3_0_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_3_1(3);
    vector<unsigned> keyIndices_3_1_0;
    AuxiliaryMap toAddMap_3_1_0(0, &keyIndices_3_1_0);
    auxMapsByAtom_3[3].push_back(&toAddMap_3_1_0);
    auxMaps_3_1[0]= &toAddMap_3_1_0;
    pair<unsigned, unsigned> value_3_1_1_0(1, 0);
    joinIndices_3_1[1][0]=value_3_1_1_0;
    vector<unsigned> keyIndices_3_1_1;
    keyIndices_3_1_1.push_back(0);
    AuxiliaryMap toAddMap_3_1_1(1, &keyIndices_3_1_1);
    auxMapsByAtom_3[2].push_back(&toAddMap_3_1_1);
    auxMaps_3_1[1]= &toAddMap_3_1_1;
    pair<unsigned, unsigned> value_3_1_2_0(0, 0);
    joinIndices_3_1[2][0]=value_3_1_2_0;
    vector<unsigned> keyIndices_3_1_2;
    keyIndices_3_1_2.push_back(0);
    AuxiliaryMap toAddMap_3_1_2(1, &keyIndices_3_1_2);
    auxMapsByAtom_3[0].push_back(&toAddMap_3_1_2);
    auxMaps_3_1[2]= &toAddMap_3_1_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_3_2(3);
    pair<unsigned, unsigned> value_3_2_0_0(0, 0);
    joinIndices_3_2[0][0]=value_3_2_0_0;
    vector<unsigned> keyIndices_3_2_0;
    keyIndices_3_2_0.push_back(0);
    AuxiliaryMap toAddMap_3_2_0(1, &keyIndices_3_2_0);
    auxMapsByAtom_3[3].push_back(&toAddMap_3_2_0);
    auxMaps_3_2[0]= &toAddMap_3_2_0;
    vector<unsigned> keyIndices_3_2_1;
    AuxiliaryMap toAddMap_3_2_1(0, &keyIndices_3_2_1);
    auxMapsByAtom_3[1].push_back(&toAddMap_3_2_1);
    auxMaps_3_2[1]= &toAddMap_3_2_1;
    pair<unsigned, unsigned> value_3_2_2_0(2, 0);
    joinIndices_3_2[2][0]=value_3_2_2_0;
    vector<unsigned> keyIndices_3_2_2;
    keyIndices_3_2_2.push_back(0);
    AuxiliaryMap toAddMap_3_2_2(1, &keyIndices_3_2_2);
    auxMapsByAtom_3[0].push_back(&toAddMap_3_2_2);
    auxMaps_3_2[2]= &toAddMap_3_2_2;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_3_3(3);
    pair<unsigned, unsigned> value_3_3_0_0(0, 0);
    joinIndices_3_3[0][0]=value_3_3_0_0;
    vector<unsigned> keyIndices_3_3_0;
    keyIndices_3_3_0.push_back(0);
    AuxiliaryMap toAddMap_3_3_0(1, &keyIndices_3_3_0);
    auxMapsByAtom_3[2].push_back(&toAddMap_3_3_0);
    auxMaps_3_3[0]= &toAddMap_3_3_0;
    vector<unsigned> keyIndices_3_3_1;
    AuxiliaryMap toAddMap_3_3_1(0, &keyIndices_3_3_1);
    auxMapsByAtom_3[1].push_back(&toAddMap_3_3_1);
    auxMaps_3_3[1]= &toAddMap_3_3_1;
    pair<unsigned, unsigned> value_3_3_2_0(2, 0);
    joinIndices_3_3[2][0]=value_3_3_2_0;
    vector<unsigned> keyIndices_3_3_2;
    keyIndices_3_3_2.push_back(0);
    AuxiliaryMap toAddMap_3_3_2(1, &keyIndices_3_3_2);
    auxMapsByAtom_3[0].push_back(&toAddMap_3_3_2);
    auxMaps_3_3[2]= &toAddMap_3_3_2;
    for(unsigned i=0;i<program.size();i++) {
        if(!wQiMap.count(program[i].getPredicateName())) {
            program[i].print();
            cout<<".\n";
        }
        else {
            vector<unsigned> tuple = program[i].getIntTuple();
            wQiMap[program[i].getPredicateName()]->insert(tuple);
        }
    }
    WQi* negWS_0[4];
    WQi* negWS_1[4];
    WQi* negWS_2[4];
    WQi* negWS_3[4];
    while(wpos.size() || wsquare.size()) {
        while(wpos.size()) {
            vector<unsigned> tuple;
            tuple.reserve(3);
            wpos.next(tuple);
            wpos.remove(tuple);
            rpos.insert(tuple);
            performJoin_0(0, NULL, &roverl, joinOrders[0][0], tuple, auxMaps_0_0, joinIndices_0_0, auxMapsByAtom_0[0], negativeLiterals_0, negWS_0);
            performJoin_0(2, NULL, &roverl, joinOrders[0][2], tuple, auxMaps_0_2, joinIndices_0_2, auxMapsByAtom_0[2], negativeLiterals_0, negWS_0);
            performJoin_1(0, NULL, &roverl, joinOrders[1][0], tuple, auxMaps_1_0, joinIndices_1_0, auxMapsByAtom_1[0], negativeLiterals_1, negWS_1);
            performJoin_1(2, NULL, &roverl, joinOrders[1][2], tuple, auxMaps_1_2, joinIndices_1_2, auxMapsByAtom_1[2], negativeLiterals_1, negWS_1);
            performJoin_2(0, NULL, &roverl, joinOrders[2][0], tuple, auxMaps_2_0, joinIndices_2_0, auxMapsByAtom_2[0], negativeLiterals_2, negWS_2);
            performJoin_2(2, NULL, &roverl, joinOrders[2][2], tuple, auxMaps_2_2, joinIndices_2_2, auxMapsByAtom_2[2], negativeLiterals_2, negWS_2);
            performJoin_3(0, NULL, &roverl, joinOrders[3][0], tuple, auxMaps_3_0, joinIndices_3_0, auxMapsByAtom_3[0], negativeLiterals_3, negWS_3);
            performJoin_3(2, NULL, &roverl, joinOrders[3][2], tuple, auxMaps_3_2, joinIndices_3_2, auxMapsByAtom_3[2], negativeLiterals_3, negWS_3);
        }
        while(wsquare.size()) {
            vector<unsigned> tuple;
            tuple.reserve(2);
            wsquare.next(tuple);
            wsquare.remove(tuple);
            rsquare.insert(tuple);
            performJoin_0(1, NULL, &roverl, joinOrders[0][1], tuple, auxMaps_0_1, joinIndices_0_1, auxMapsByAtom_0[1], negativeLiterals_0, negWS_0);
            performJoin_0(3, NULL, &roverl, joinOrders[0][3], tuple, auxMaps_0_3, joinIndices_0_3, auxMapsByAtom_0[3], negativeLiterals_0, negWS_0);
            performJoin_1(1, NULL, &roverl, joinOrders[1][1], tuple, auxMaps_1_1, joinIndices_1_1, auxMapsByAtom_1[1], negativeLiterals_1, negWS_1);
            performJoin_1(3, NULL, &roverl, joinOrders[1][3], tuple, auxMaps_1_3, joinIndices_1_3, auxMapsByAtom_1[3], negativeLiterals_1, negWS_1);
            performJoin_2(1, NULL, &roverl, joinOrders[2][1], tuple, auxMaps_2_1, joinIndices_2_1, auxMapsByAtom_2[1], negativeLiterals_2, negWS_2);
            performJoin_2(3, NULL, &roverl, joinOrders[2][3], tuple, auxMaps_2_3, joinIndices_2_3, auxMapsByAtom_2[3], negativeLiterals_2, negWS_2);
            performJoin_3(1, NULL, &roverl, joinOrders[3][1], tuple, auxMaps_3_1, joinIndices_3_1, auxMapsByAtom_3[1], negativeLiterals_3, negWS_3);
            performJoin_3(3, NULL, &roverl, joinOrders[3][3], tuple, auxMaps_3_3, joinIndices_3_3, auxMapsByAtom_3[3], negativeLiterals_3, negWS_3);
        }
    }
    for (map<string, RQi*>::iterator it = rQiMap.begin(); it != rQiMap.end(); it++) {
        it->second->printTuples(it->first);
    }
}
