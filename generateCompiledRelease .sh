#!/bin/sh

FOLDER="compiledRelease/"
rm -r $FOLDER
mkdir $FOLDER
cp aspcppcompilation DynamicLibraryMake Executor.h WQi.o WQi.h RQi.o RQi.h AuxiliaryMap.o AuxiliaryMap.h Literal.o Literal.h ConstantsManager.o ConstantsManager.h Atom.o Atom.h SharedFunctions.o SharedFunctions.h AspCore2InstanceBuilder.o AspCore2InstanceBuilder.h InputDirector.o $FOLDER
cp -r DLV2libs $FOLDER
