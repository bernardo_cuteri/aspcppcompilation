#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;


int main(int argc, char**argv) {

	int numberOfFacts = atoi(argv[1]);
	int maxTerm = atoi(argv[2]);
	srand(time(0));

	numberOfFacts = rand()%numberOfFacts+1;
	maxTerm = rand()%maxTerm+1;
	for(int i=0;i<numberOfFacts;i++) {
		cout<<"eq("<<rand()%maxTerm<<","<<rand()%maxTerm<<").\n";
	}
	return 0;
}
