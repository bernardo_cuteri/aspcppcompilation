#include "Executor.h"

#include "ConstantsManager.h"

#include "DLV2libs/input/InputDirector.h"

#include "AspCore2InstanceBuilder.h"

extern "C" Executor* create_object() {
    return new Executor;
}
extern "C" void destroy_object( Executor* object ) {
    delete object;
}
Executor::~Executor() {}

Executor::Executor() {}

void performJoin_0(unsigned index, WQi* wqi, RQi* rqi, const vector<unsigned> & joinOrder, const vector<unsigned> & tuple0, AuxiliaryMap** auxiliaryMaps, vector<map<unsigned, pair<unsigned, unsigned> > > & keys_indices, vector<AuxiliaryMap* > & atomMaps, const vector<bool> & negativeLiterals, string ruleBodyPredicates[], std::vector<std::vector<aspc::Literal> > & failedConstraints, const vector<unsigned> & bodyArieties, const vector<map<unsigned, string> > & bodyConstantsOfNegLit, WQi* wQiNeg[]) {
    for(AuxiliaryMap* p: atomMaps) {
        p->insert2(tuple0);
    }
    if(negativeLiterals[index]) {
        return;
    }
    const vector<unsigned>* tupleStack[3];
    tupleStack[index]=&tuple0;
    AuxiliaryMap* auxMap = auxiliaryMaps[0];
    vector<unsigned> key_0;
    for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[0].begin();it != keys_indices[0].end();it++) {
        key_0.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
    }
    if(!negativeLiterals[joinOrder[1]] || !wQiNeg[joinOrder[1]]->contains(key_0)){
        vector<vector<unsigned> >* matchingTuples_0=auxMap->getValues(key_0);
        if((matchingTuples_0!=NULL && !negativeLiterals[joinOrder[1]]) || (matchingTuples_0==NULL && negativeLiterals[joinOrder[1]])) {
            bool negative0 = (matchingTuples_0 == NULL);
            if(negative0) {
                matchingTuples_0 = new vector<vector<unsigned> >(1);
            }
            for(const vector<unsigned> & tuple1: *matchingTuples_0) {
                tupleStack[joinOrder[1]]=&tuple1;
                AuxiliaryMap* auxMap = auxiliaryMaps[1];
                vector<unsigned> key_1;
                for(map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[1].begin();it != keys_indices[1].end();it++) {
                    key_1.push_back((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                }
                if(!negativeLiterals[joinOrder[2]] || !wQiNeg[joinOrder[2]]->contains(key_1)){
                    vector<vector<unsigned> >* matchingTuples_1=auxMap->getValues(key_1);
                    if((matchingTuples_1!=NULL && !negativeLiterals[joinOrder[2]]) || (matchingTuples_1==NULL && negativeLiterals[joinOrder[2]])) {
                        bool negative1 = (matchingTuples_1 == NULL);
                        if(negative1) {
                            matchingTuples_1 = new vector<vector<unsigned> >(1);
                        }
                        for(const vector<unsigned> & tuple2: *matchingTuples_1) {
                            tupleStack[joinOrder[2]]=&tuple2;
                            if((*tupleStack[joinOrder[0]])[0]!=(*tupleStack[joinOrder[1]])[0] && (*tupleStack[joinOrder[1]])[0]!=(*tupleStack[joinOrder[0]])[1] && (*tupleStack[joinOrder[0]])[0]!=(*tupleStack[joinOrder[0]])[1]) {
                                vector<aspc::Literal>failedConstraint;
                                for(unsigned i=0;i<3;i++) {
                                    const vector<unsigned>* tuple=tupleStack[joinOrder[i]];
                                    vector<string> terms;
                                    if(!tuple->empty()) {
                                        for(unsigned v:*tuple) {
                                            terms.push_back(ConstantsManager::getInstance().unmapConstant(v));
                                        }
                                    }
                                    else {
                                        terms.resize(bodyArieties[joinOrder[i]]);
                                        for(map<unsigned, string>::const_iterator it=bodyConstantsOfNegLit[joinOrder[i]].begin();it!=bodyConstantsOfNegLit[joinOrder[i]].end();it++) {
                                            terms[it->first]=it->second;
                                        }
                                        for (std::map<unsigned, pair<unsigned, unsigned> >::iterator it = keys_indices[i-1].begin(); it != keys_indices[i-1].end(); it++) {
                                            terms[it->first]=ConstantsManager::getInstance().unmapConstant((*tupleStack[joinOrder[it->second.first]])[it->second.second]);
                                        }
                                    }
                                    aspc::Atom toAddAtom(ruleBodyPredicates[joinOrder[i]], terms);
                                    aspc::Literal toAddLiteral(negativeLiterals[joinOrder[i]], toAddAtom);
                                    failedConstraint.push_back(toAddLiteral);
                                }
                                failedConstraints.push_back(failedConstraint);
                            }
                        }
                        if(negative1) {
                            delete matchingTuples_1;
                        }
                    }
                }
            }
            if(negative0) {
                delete matchingTuples_0;
            }
        }
    }
}

void Executor::executeFromFile(const char* filename) {
    DLV2::InputDirector director;
    AspCore2InstanceBuilder* builder = new AspCore2InstanceBuilder();
    director.configureBuilder(builder);
    vector<const char*> fileNames;
    fileNames.push_back(filename);
    director.parse(fileNames);
    executeProgramOnFacts(builder->getProblemInstance());
    delete builder;
}
void Executor::executeProgramOnFacts(const vector<aspc::Atom> & program) {
    failedConstraints.erase(failedConstraints.begin(), failedConstraints.end());
    vector<vector<vector<unsigned> > > joinOrders(1);
    joinOrders[0].resize(3);
    joinOrders[0][0].push_back(0);
    joinOrders[0][0].push_back(2);
    joinOrders[0][0].push_back(1);
    joinOrders[0][1].push_back(1);
    joinOrders[0][1].push_back(2);
    joinOrders[0][1].push_back(0);
    joinOrders[0][2].push_back(2);
    joinOrders[0][2].push_back(1);
    joinOrders[0][2].push_back(0);
    map<string, WQi*> wQiMap;
    map<string, RQi*> rQiMap;
    WQi weq(2, 2);
    wQiMap["eq"] = &weq;
    RQi req(2, 2);
    rQiMap["eq"] = &req;
    string ruleBodyPredicates_0[] = {"eq", "eq", "eq"};
    vector<bool> negativeLiterals_0;
    vector<unsigned> bodyArieties_0;
    vector<map<unsigned,string> > bodyConstantsOfNegLit_0(3);
    negativeLiterals_0.push_back(true);
    bodyArieties_0.push_back(2);
    negativeLiterals_0.push_back(false);
    bodyArieties_0.push_back(2);
    negativeLiterals_0.push_back(false);
    bodyArieties_0.push_back(2);
    AuxiliaryMap* auxMaps_0_0[2];
    AuxiliaryMap* auxMaps_0_1[2];
    AuxiliaryMap* auxMaps_0_2[2];
    vector<vector<AuxiliaryMap*> > auxMapsByAtom_0(3);
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_0_0(2);
    vector<unsigned> keyIndices_0_0_0;
    AuxiliaryMap toAddMap_0_0_0(0, &keyIndices_0_0_0);
    auxMapsByAtom_0[2].push_back(&toAddMap_0_0_0);
    auxMaps_0_0[0]= &toAddMap_0_0_0;
    pair<unsigned, unsigned> value_0_0_1_0(1, 1);
    joinIndices_0_0[1][0]=value_0_0_1_0;
    vector<unsigned> keyIndices_0_0_1;
    keyIndices_0_0_1.push_back(0);
    AuxiliaryMap toAddMap_0_0_1(1, &keyIndices_0_0_1);
    auxMapsByAtom_0[1].push_back(&toAddMap_0_0_1);
    auxMaps_0_0[1]= &toAddMap_0_0_1;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_0_1(2);
    pair<unsigned, unsigned> value_0_1_0_1(0, 0);
    joinIndices_0_1[0][1]=value_0_1_0_1;
    vector<unsigned> keyIndices_0_1_0;
    keyIndices_0_1_0.push_back(1);
    AuxiliaryMap toAddMap_0_1_0(1, &keyIndices_0_1_0);
    auxMapsByAtom_0[2].push_back(&toAddMap_0_1_0);
    auxMaps_0_1[0]= &toAddMap_0_1_0;
    pair<unsigned, unsigned> value_0_1_1_0(1, 0);
    joinIndices_0_1[1][0]=value_0_1_1_0;
    pair<unsigned, unsigned> value_0_1_1_1(0, 1);
    joinIndices_0_1[1][1]=value_0_1_1_1;
    vector<unsigned> keyIndices_0_1_1;
    keyIndices_0_1_1.push_back(0);
    keyIndices_0_1_1.push_back(1);
    AuxiliaryMap toAddMap_0_1_1(2, &keyIndices_0_1_1);
    auxMapsByAtom_0[0].push_back(&toAddMap_0_1_1);
    auxMaps_0_1[1]= &toAddMap_0_1_1;
    vector<map<unsigned, pair<unsigned,unsigned> > > joinIndices_0_2(2);
    pair<unsigned, unsigned> value_0_2_0_0(0, 1);
    joinIndices_0_2[0][0]=value_0_2_0_0;
    vector<unsigned> keyIndices_0_2_0;
    keyIndices_0_2_0.push_back(0);
    AuxiliaryMap toAddMap_0_2_0(1, &keyIndices_0_2_0);
    auxMapsByAtom_0[1].push_back(&toAddMap_0_2_0);
    auxMaps_0_2[0]= &toAddMap_0_2_0;
    pair<unsigned, unsigned> value_0_2_1_0(0, 0);
    joinIndices_0_2[1][0]=value_0_2_1_0;
    pair<unsigned, unsigned> value_0_2_1_1(1, 1);
    joinIndices_0_2[1][1]=value_0_2_1_1;
    vector<unsigned> keyIndices_0_2_1;
    keyIndices_0_2_1.push_back(0);
    keyIndices_0_2_1.push_back(1);
    AuxiliaryMap toAddMap_0_2_1(2, &keyIndices_0_2_1);
    auxMapsByAtom_0[0].push_back(&toAddMap_0_2_1);
    auxMaps_0_2[1]= &toAddMap_0_2_1;
    for(unsigned i=0;i<program.size();i++) {
        if(!wQiMap.count(program[i].getPredicateName())) {
        }
        else {
            vector<unsigned> tuple = program[i].getIntTuple();
            wQiMap[program[i].getPredicateName()]->insert(tuple);
        }
    }
    WQi* negWS_0[3];
    negWS_0[0]=wQiMap["eq"];
    while(weq.size()) {
        while(weq.size()) {
            vector<unsigned> tuple;
            tuple.reserve(2);
            weq.next(tuple);
            weq.remove(tuple);
            if(tuple[0]!=tuple[1]) {
                performJoin_0(0, NULL, NULL, joinOrders[0][0], tuple, auxMaps_0_0, joinIndices_0_0, auxMapsByAtom_0[0], negativeLiterals_0, ruleBodyPredicates_0, failedConstraints, bodyArieties_0, bodyConstantsOfNegLit_0, negWS_0);
            }
            if(tuple[0]!=tuple[1]) {
                performJoin_0(1, NULL, NULL, joinOrders[0][1], tuple, auxMaps_0_1, joinIndices_0_1, auxMapsByAtom_0[1], negativeLiterals_0, ruleBodyPredicates_0, failedConstraints, bodyArieties_0, bodyConstantsOfNegLit_0, negWS_0);
            }
            if(tuple[0]!=tuple[1]) {
                performJoin_0(2, NULL, NULL, joinOrders[0][2], tuple, auxMaps_0_2, joinIndices_0_2, auxMapsByAtom_0[2], negativeLiterals_0, ruleBodyPredicates_0, failedConstraints, bodyArieties_0, bodyConstantsOfNegLit_0, negWS_0);
            }
        }
    }
}
