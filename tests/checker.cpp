#include <iostream>
#include <fstream>

using namespace std;

bool is_empty(std::ifstream& pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}

int main() {

	ifstream dlv("dlv.out");
	ifstream aspcpp("aspcpp.out");
	bool emptyDlv = is_empty(dlv);
	bool emptyAspcpp = is_empty(aspcpp);
	if((emptyDlv && !emptyAspcpp) || (!emptyDlv && emptyAspcpp)) {
		cout<<"OK"<<endl;
	}
	else {
		cout<<"ERRORE"<<endl;
		cerr<<"ERROR ENCOUNTERED"<<endl;
	}
	return 0;
}
