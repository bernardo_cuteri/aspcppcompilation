/*
 *
 *  Copyright 2016 Bernardo Cuteri, Francesco Ricca.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include "CompilationManager.h"
#include "ExecutionManager.h"
#include "LazyConstraint.h"
#include "LazyConstraintImpl.h"

extern string executablePath;

bool cmdOptionExists(const char** argv, int argc, const string & option) {
    for (int i = 0; i < argc; i++) {
        string current = argv[i];
        if (option == current) {
            return true;
        }
    }
    return false;
}

int main(const int argc, const char **argv) {

    std::ios::sync_with_stdio(false);

    string executablePathAndName = argv[0];
    executablePath = executablePathAndName;
    for (int i = executablePath.size() - 1; i >= 0; i--) {
        if (executablePath[i] == '/') {
            executablePath = executablePath.substr(0, i);
            break;
        }
    }

    if (cmdOptionExists(argv, argc, "-e")) {
        ExecutionManager execManager;
        execManager.compileDynamicLibrary();
        execManager.launchExecutorOnFile(argv[1]);
        //execManager.parseFactsAndExecute("Instances/constraint");
        return 0;
    }

    /*Graph g(5);
    g.addEdge(1, 0);
    g.addEdge(0, 2);
    g.addEdge(2, 1);
    g.addEdge(0, 3);
    g.addEdge(3, 4);
 
    cout << "Following are strongly connected components in "
            "given graph \n";
    g.printSCCs();
    return 0;
     */



    //atoms table

    /*vector<string> atoms;
    atoms.push_back("");
    atoms.push_back("b(1)");
    atoms.push_back("b(2)");
    atoms.push_back("f(1)");
    atoms.push_back("f(2)");
    atoms.push_back("");
    atoms.push_back("c(ciao,1,1)");
    atoms.push_back("c(ciao,2,2)");
    atoms.push_back("c(ciao,2,1)");
    atoms.push_back("c(ciao,1,2)");
     * */
    
    /*
    vector<string> atoms;
    atoms.push_back("eq(5,2)");
    atoms.push_back("eq(0,5)");
    atoms.push_back("eq(5,3)");
    
    //atoms.push_back("eq(a,c)");
    //atoms.push_back("eq(b,d)");

    //test answer set, all facts are true is this example
    vector<unsigned int> interpretation(atoms.size(), 1);
    //interpretation[2]=0;
    //interpretation[atoms.size() - 1] = 0;

    //Use the lazy constraint
    LazyConstraint* constraint = new LazyConstraintImpl();
    //give the program encoding as a file
    constraint->setFilename(executablePath + "/encodings/constraint");
    //give the atoms table
    std::vector<unsigned> watchedAtoms;
    
    //vector<string> atomsTable;
    //atomsTable.push_back("eq(a,b)");
    constraint->onAtoms(atoms, watchedAtoms);

   
    //if some constraints were violated
    if (!constraint->onAnswerSet(interpretation)) {
        //data structure taken as reference and used to store failed constraints
        std::vector<std::vector<int> > failedConstraints;
        constraint->onCheckFail(failedConstraints);

        //print failed constraints
        for (unsigned i = 0; i < failedConstraints.size(); i++) {
            for (unsigned j = 0; j < failedConstraints[i].size(); j++) {
                cout << failedConstraints[i][j] << " ";
            }
            cout << "\n";
        }
    }
    delete constraint;
    return 0;
    
     */

    std::ofstream outfile(executablePath + "/Executor.cpp");
    //CompilationManager manager(&std::cout);
    CompilationManager manager;
    manager.setOutStream(&outfile);
    manager.compileFile(argv[1]);
    outfile.close();
    ExecutionManager execManager;
    execManager.compileDynamicLibrary();
    if (argc > 2) {
        execManager.launchExecutorOnFile(argv[2]);
    }
    return 0;

}