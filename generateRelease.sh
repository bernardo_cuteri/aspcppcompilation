#!/bin/sh

FOLDER="release/"
rm -r $FOLDER
mkdir $FOLDER
cp DynamicLibraryMake Executor.h WQi.* RQi.* AuxiliaryMap.* Literal.* Rule.* Program.* ConstantsManager.* Atom.* SharedFunctions.* AspCore2InstanceBuilder.* AspCore2ProgramBuilder.* CompilationManager.* ExecutionManager.* Indentation.* LazyConstraint* $FOLDER
cp -r DLV2libs $FOLDER
