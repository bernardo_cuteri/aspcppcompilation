set terminal png
set output "cactus_three_sat.png"
set xlabel "Solved Instances"
set ylabel "Time (s)"
plot "eager.dat" using 2:1 w lp ,"lazy.dat" using 2:1 w lp ,"waspOrig.dat" using 2:1 w lp
