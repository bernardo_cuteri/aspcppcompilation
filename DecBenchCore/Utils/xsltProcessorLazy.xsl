<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html> 
<body>
  <h2>Results</h2>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th style="text-align:left">Solver</th>
      <th style="text-align:left">Problem</th>
      <th style="text-align:left">Total Time</th>
      <th style="text-align:left">Clauses</th>
      <th style="text-align:left">Variable</th>
      <!--<th style="text-align:left">Clauses</th>-->
    </tr>
    <xsl:for-each-group select="benchmarkSuite/solver" group-by="name">
        <xsl:for-each-group select="current-group()/problem/benchmark" group-by="clauses">
            <xsl:for-each-group select="current-group()" group-by="variables">
                <tr>             
                  <td><xsl:value-of select="../../name"/></td>
                  <td><xsl:value-of select="../name"/></td>
                  <td><xsl:value-of select="sum(current-group()/cpuTime)"/></td>
                  <td><xsl:value-of select="clauses"/></td>
                  <td><xsl:value-of select="current-grouping-key()"/></td>
                </tr>     
                
            </xsl:for-each-group>
        </xsl:for-each-group>
    </xsl:for-each-group>
  </table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>


