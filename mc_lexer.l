%{
/* C++ string header, for string ops below */
#include <string>
/* Implementation of yyFlexScanner */ 
#include "mc_scanner.h"

/* typedef to make the returns for the tokens shorter */
typedef yy::MC_Parser::token token;

/* define to keep from re-typing the same code over and over */
#define STOKEN( x ) ( new std::string( x ) )

/* define yyterminate as this instead of NULL */
#define yyterminate() return( token::END )

/* msvc2010 requires that we exclude this header file. */
#define YY_NO_UNISTD_H

%}

%option debug 
%option nodefault 
%option yyclass="MC_Scanner" 
%option noyywrap 
%option c++

%%

:-          {   
               return( token::IMPLICATION );      
            }
\.          {   
               return( token::DOT );      
            }

\<\=          {   
               return( token::LTE );      
            }
>\=          {   
               return( token::GTE );      
            }
\<           {   
               return( token::LT );      
            }
\>           {   
               return( token::GT );      
            }
!\=          {   
               return( token::NE );      
            }

,           {   
               return( token::COMMA );      
            }

\(          {   
               return( token::OB );      
            }

\)          {   
               return( token::CB );      
            }

not         {   
               return( token::NOT );      
            }

\|          {   
               return( token::PIPE );      
            }
_           {   
               return( token::WILD_CARD );      
            }

[a-zA-Z0-9][a-zA-Z0-9_]*   { 
               yylval->sval = STOKEN( yytext );  
               return( token::WORD );      
            }

\n          {}

.           {}
%%