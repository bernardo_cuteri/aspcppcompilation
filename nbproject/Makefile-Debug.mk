#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/ArithmeticExpression.o \
	${OBJECTDIR}/AspCore2InstanceBuilder.o \
	${OBJECTDIR}/AspCore2ProgramBuilder.o \
	${OBJECTDIR}/Atom.o \
	${OBJECTDIR}/AuxiliaryMap.o \
	${OBJECTDIR}/CompilationManager.o \
	${OBJECTDIR}/ConstantsManager.o \
	${OBJECTDIR}/DLV2libs/input/InputDirector.o \
	${OBJECTDIR}/ExecutionManager.o \
	${OBJECTDIR}/Indentation.o \
	${OBJECTDIR}/LazyConstraintImpl.o \
	${OBJECTDIR}/Literal.o \
	${OBJECTDIR}/Program.o \
	${OBJECTDIR}/RQi.o \
	${OBJECTDIR}/Rule.o \
	${OBJECTDIR}/SharedFunctions.o \
	${OBJECTDIR}/WQi.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-Wall -Wextra
CXXFLAGS=-Wall -Wextra

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk aspcppcompilation

aspcppcompilation: ${OBJECTFILES}
	${LINK.cc} -o aspcppcompilation ${OBJECTFILES} ${LDLIBSOPTIONS} -ldl

${OBJECTDIR}/ArithmeticExpression.o: ArithmeticExpression.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ArithmeticExpression.o ArithmeticExpression.cpp

${OBJECTDIR}/AspCore2InstanceBuilder.o: AspCore2InstanceBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/AspCore2InstanceBuilder.o AspCore2InstanceBuilder.cpp

${OBJECTDIR}/AspCore2ProgramBuilder.o: AspCore2ProgramBuilder.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/AspCore2ProgramBuilder.o AspCore2ProgramBuilder.cpp

${OBJECTDIR}/Atom.o: Atom.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Atom.o Atom.cpp

${OBJECTDIR}/AuxiliaryMap.o: AuxiliaryMap.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/AuxiliaryMap.o AuxiliaryMap.cpp

${OBJECTDIR}/CompilationManager.o: CompilationManager.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CompilationManager.o CompilationManager.cpp

${OBJECTDIR}/ConstantsManager.o: ConstantsManager.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ConstantsManager.o ConstantsManager.cpp

${OBJECTDIR}/DLV2libs/input/InputDirector.o: DLV2libs/input/InputDirector.cpp
	${MKDIR} -p ${OBJECTDIR}/DLV2libs/input
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DLV2libs/input/InputDirector.o DLV2libs/input/InputDirector.cpp

${OBJECTDIR}/ExecutionManager.o: ExecutionManager.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ExecutionManager.o ExecutionManager.cpp

${OBJECTDIR}/Indentation.o: Indentation.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Indentation.o Indentation.cpp

${OBJECTDIR}/LazyConstraintImpl.o: LazyConstraintImpl.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LazyConstraintImpl.o LazyConstraintImpl.cpp

${OBJECTDIR}/Literal.o: Literal.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Literal.o Literal.cpp

${OBJECTDIR}/Program.o: Program.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Program.o Program.cpp

${OBJECTDIR}/RQi.o: RQi.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RQi.o RQi.cpp

${OBJECTDIR}/Rule.o: Rule.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Rule.o Rule.cpp

${OBJECTDIR}/SharedFunctions.o: SharedFunctions.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SharedFunctions.o SharedFunctions.cpp

${OBJECTDIR}/WQi.o: WQi.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/WQi.o WQi.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
