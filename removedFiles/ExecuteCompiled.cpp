#include "ExecutionManager.h"
#include <string>
#include <iostream>

extern std::string executablePath;

int main(const int argc, const char **argv) {

	std::ios::sync_with_stdio(false);
    
    std::string executablePathAndName = argv[0];
    executablePath = executablePathAndName;
    for(int i=executablePath.size()-1;i>=0;i--) {
        if(executablePath[i]=='/') {
            executablePath=executablePath.substr(0, i);
            break;
        }
    }
    ExecutionManager execManager;
    execManager.parseFactsAndExecute(argv[1]);
    return 0;    
}
