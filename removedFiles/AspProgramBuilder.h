#ifndef BUILDERDIRECTOR_H
#define	BUILDERDIRECTOR_H

#include <string>
#include <map>
#include "Program.h"
using namespace std;

class AspProgramBuilder {
public:
    AspProgramBuilder();
    ~AspProgramBuilder();
    void addAtom(string);
    void addTerm(string);
    void addInequality();
    void addInequalitySign(InequalitySign type);
    void implicationSignReached();
    void addImplicationRule();
    void addFact();
    void dotReached();
    void negationFound();
    Program* getProgram();
    map<string, int>* getArietyMap();
private:
    Program* program;
    vector<string>* buildingTerms;
    vector<Literal*>* buildingBody;
    vector<Atom*>* buildingHead;
    bool buildingHeadPhase;
    map<string, int>* arietyMap;
    InequalitySign inequalitySign;
    vector<tuple<string, InequalitySign, string> > inequalities;


    void validatePredicateName(string name);
    void validateTermString(string term);
    void validateImplicationRule();


};

#endif	/* BUILDERDIRECTOR_H */

