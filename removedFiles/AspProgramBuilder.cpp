#include "AspProgramBuilder.h"
#include <cassert>
#include <iostream>
#include <set>
#include <cstdlib>
#include <tuple>


using namespace std;

AspProgramBuilder::AspProgramBuilder() : buildingHeadPhase(true),
buildingHead(new vector<Atom*>), buildingBody(new vector<Literal*>),
buildingTerms(new vector<string>), program(new Program()),
arietyMap(new map<string, int>()) {
}

AspProgramBuilder::~AspProgramBuilder() {
    delete arietyMap;
    delete buildingHead;
    delete buildingBody;
    delete buildingTerms;
    delete program;
}

void AspProgramBuilder::addAtom(string predicateName) {
    validatePredicateName(predicateName);
    if (buildingHeadPhase) {
        buildingHead->push_back(new Atom(predicateName, buildingTerms));
    } else {
        buildingBody->push_back(new Literal(false, new Atom(predicateName, buildingTerms)));
    }
    if (arietyMap->count(predicateName)) {
        assert((*arietyMap)[predicateName] == buildingTerms->size());
    } else {
        (*arietyMap)[predicateName] = buildingTerms->size();
    }
    buildingTerms = new vector<string>;
    //cout << "atom " << predicateName << "\n";
}

void AspProgramBuilder::addTerm(string term) {
    validateTermString(term);
    buildingTerms->push_back(term);
    //cout << "term " << term << "\n";
}

void AspProgramBuilder::addFact() {
    program->addFact(buildingHead->back());
    buildingHead->clear();
    //cout << "add fact\n";
}

void AspProgramBuilder::implicationSignReached() {
    buildingHeadPhase = false;
    //cout << "implication\n";
}

void AspProgramBuilder::addImplicationRule() {
    validateImplicationRule();
    program->addRule(new Rule(buildingHead, buildingBody, inequalities));
    buildingBody = new vector<Literal*>();
    buildingHead = new vector<Atom*>();
    inequalities.clear();
    //cout << "implication rule\n";
}

Program* AspProgramBuilder::getProgram() {
    return program;
}

void AspProgramBuilder::dotReached() {
    buildingHeadPhase = true;
}

void AspProgramBuilder::negationFound() {
    buildingBody->back()->negate();
}

map<string, int>* AspProgramBuilder::getArietyMap() {
    return arietyMap;
}

void AspProgramBuilder::validatePredicateName(string name) {
    if (!(name[0] >= 'a' && name[0] <= 'z' || name[0] >= 'A' && name[0] <= 'Z')) {
        cerr << "Invalid atom name " << name << endl;
        assert(false);
    }

}

void AspProgramBuilder::validateTermString(string term) {
    if (buildingHeadPhase) {
        if (term == "_") {
            cerr << "Invalid wild card usage in rule head\n";
            exit(EXIT_FAILURE);
        }
    }

}

void AspProgramBuilder::validateImplicationRule() {
    //    if (buildingBody->size() > 2) {
    //        cerr << "At most two literals in bodies\n";
    //        exit(EXIT_FAILURE);
    //    }
    set<string> positiveBodyAtomsTerms;
    for (int i = 0; i < buildingBody->size(); i++) {
        Literal* current = (*buildingBody)[i];
        if (!current->isNegated()) {
            for (int j = 0; j < current->getAriety(); j++) {
                if (current->isVariableTermAt(j)) {
                    positiveBodyAtomsTerms.insert(current->getTermAt(j));
                }
            }
        }
    }
    for (int i = 0; i < buildingBody->size(); i++) {
        if (!(*buildingBody)[i]->isNegated()) {
            for (int j = 0; j < (*buildingBody)[i]->getAriety(); j++) {
                if ((*buildingBody)[i]->isVariableTermAt(j) && !positiveBodyAtomsTerms.count((*buildingBody)[i]->getTermAt(j))) {
                    cerr << "Rule is not safe\n";
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
}
InequalitySign getSymmetricalSign(InequalitySign sign) {
    switch(sign) {
        case LT:
            return GT;
            break;
        case LTE:
            return GTE;
            break;
        case GT:
            return LT;
            break;
        case GTE:
            return LTE;
            break;
        case NE:
            return NE;
            break;
    }
    assert(false);
}
void AspProgramBuilder::addInequality() {
    if (buildingTerms->at(0)[0] >= 'A' && buildingTerms->at(0)[0] <= 'Z') {
        tuple<string, InequalitySign, string> inequality(buildingTerms->at(0), inequalitySign, buildingTerms->at(1));
        inequalities.push_back(inequality);
    }
    else {
        tuple<string, InequalitySign, string> inequality(buildingTerms->at(1), getSymmetricalSign(inequalitySign), buildingTerms->at(0));
        inequalities.push_back(inequality);
    }

    buildingTerms->clear();
    //cout<<get<0>(inequality)<<" "<<get<1>(inequality)<<" "<<get<2>(inequality)<<"\n";
}

void AspProgramBuilder::addInequalitySign(InequalitySign sign) {
    inequalitySign = sign;
}