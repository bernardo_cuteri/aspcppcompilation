#ifndef WQI_H
#define	WQI_H
#include <list>
#include <vector>
#include <string>
using namespace std;

class WQi {
public:
    enum MatchingType: int_fast8_t{INT, STRING, BOTH};
    WQi(int height, int predicateAriety);
    void insert(vector<int> & value);
    virtual ~WQi();
    int size() const;
    void next(vector<int> & next) const;
    void pop(vector<int> & tuple);
    bool contains(vector<int> & tuple) const;
    void remove(vector<int> & tuple);
    void printTree() const;
    void printTuples(const string & predicateName) const;
    int getTupleCount() const;
protected:
    void printTuples(const string & prec, bool flag) const;
    vector<WQi*> nestedArray[2];
    vector<int> definedIdexes[2];
    vector<bool> leafTruth[2];
    int height;
    int predicateAriety;

};

#endif	/* WQI_H */

