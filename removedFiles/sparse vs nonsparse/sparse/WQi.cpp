/* 
 * File:   WQi.cpp
 * Author: bernardo
 * 
 * Created on February 26, 2016, 11:17 AM
 */

#include <vector>

#include "WQi.h"
#include "ConstantsManager.h"
#include <iostream>
#include <algorithm>
#include <climits>

const int halfMaxInt = INT_MAX / 2;
//TODO test different initializations

WQi::WQi(int height, int predicateAriety) : height(height),
predicateAriety(predicateAriety) {

}

WQi::~WQi() {
}

void WQi::insert(vector<int>& tuple) {
    //leaf array
    int v = tuple[predicateAriety - height];
    int type = 0;
    if (v >= halfMaxInt) {
        v -= halfMaxInt;
        type = 1;
    }
    
    if (height == 1) {
        if (!leafTruth[type].count(v)) {
            leafTruth[type].insert(v);
            definedIdexes[type].push_back(v);
        }
    }//nested array
    else {
        if (!nestedArray[type].count(v)) {
            nestedArray[type][v] = new WQi(height - 1, predicateAriety);
            definedIdexes[type].push_back(v);
        }
        nestedArray[type][v]->insert(tuple);
    }

}

int WQi::size() const {
    return definedIdexes[0].size() + definedIdexes[1].size();
}

void WQi::pop(vector<int>& tuple) {
    next(tuple);
    remove(tuple);
    /*int value = definedIdexes.back();
    tuple.push_back(value);
    if (height != 1) {
        WQi* succ = nestedArray[value];
        succ -> pop(tuple);
        if (!succ->size()) {
            delete succ;
            nestedArray[value] = 0;
            definedIdexes.pop_back();
        }
    } else {
        leafTruth[value] = false;
        definedIdexes.pop_back();
    }*/


}

void WQi::next(vector<int> & nextTuple) const {
    if (definedIdexes[0].size()) {
        nextTuple.push_back(definedIdexes[0].back());
        if (height > 1) {
            nestedArray[0].at(definedIdexes[0].back())->next(nextTuple);
        }
    } else {
        nextTuple.push_back(definedIdexes[1].back() + halfMaxInt);
        if (height > 1) {
            nestedArray[1].at(definedIdexes[1].back())->next(nextTuple);
        }
    }
}

//TODO do we always remove the first element according to the lists?

void WQi::remove(vector<int>& tuple) {
    int value = tuple[tuple.size() - height];
    int index = 0;
    if (value >= halfMaxInt) {
        value -= halfMaxInt;
        index = 1;
    }
    if (height > 1) {
        WQi* succ = nestedArray[index][value];
        succ -> remove(tuple);
        if (!succ->size()) {
            delete succ;
            nestedArray[index].erase(value);
            definedIdexes[index].pop_back();
        }
    } else {
        leafTruth[index].erase(value);
        definedIdexes[index].pop_back();
    }
}

bool WQi::contains(vector<int>& tuple) const {

    int i = tuple.size() - height;
    int value = tuple[i];
    int index = 0;
    if (value >= halfMaxInt) {
        value -= halfMaxInt;
        index = 1;
    }
    if (height == 1) {
        if (value >= leafTruth[index].size()) {
            return false;
        }
        return leafTruth[index].count(value);
    }
    if (!nestedArray[index].count(value)) {
        return false;
    }
    return nestedArray[index].at(value)->contains(tuple);
}

void WQi::printTree() const {
    string tab = "    ";
    for (int k = 0; k <= 1; k++) {
        for (vector<int>::const_iterator it = definedIdexes[k].begin(); it != definedIdexes[k].end(); it++) {
            for (int i = 0; i < predicateAriety - height; i++) {
                cout << tab;
            }
            cout << *it;
            cout << "\n";
            if (height != 1) {
                nestedArray[k].at(*it)->printTree();
            }
        }
    }
}

void WQi::printTuples(const string & predicateName) const {
    printTuples(predicateName + "(", true);
}

void WQi::printTuples(const string & prec, bool flag) const {
    for (int k = 0; k <= 1; k++) {
        for (vector<int>::const_iterator it = definedIdexes[k].begin(); it != definedIdexes[k].end(); it++) {
            //string next = prec + to_string(*it);
            string next = prec + ConstantsManager::getInstance().unmapConstant(*it+k*halfMaxInt);
            if (height != 1) {
                next += ",";
                nestedArray[k].at(*it)->printTuples(next, flag);
            } else {
                next += ").\n";
                cout << next;
            }
        }
    }
}

int WQi::getTupleCount() const {
    if (height == 1) {
        return definedIdexes[0].size() + definedIdexes[1].size();
    }
    int cont = 0;
    for (int k = 0; k <= 1; k++) {
        for (vector<int>::const_iterator it = definedIdexes[k].begin(); it != definedIdexes[k].end(); it++) {
            cont += nestedArray[k].at(*it)->getTupleCount();
        }
    }
    return cont;

}

