/* 
 * File:   AuxiliaryMap.cpp
 * Author: bernardo
 * 
 * Created on March 7, 2016, 2:17 PM
 */

#include <vector>

#include "AuxiliaryMap.h"
#include <iostream>
#include <map>
#include <algorithm>
#include <climits>

const int halfMaxInt = INT_MAX / 2;

AuxiliaryMap::AuxiliaryMap(int height, vector<int> * keyIndices) : height(height),
keySize(keyIndices->size()), keyIndices(keyIndices) {
    if(keySize == 0) {
        leafTuples[0][0]=new list<vector<int>*>();
    }
}


AuxiliaryMap::AuxiliaryMap() {
}


AuxiliaryMap::~AuxiliaryMap() {
}

void AuxiliaryMap::insert2(vector<int>* value) {
    //TODO  can we do better?
    if(!keySize) {
        leafTuples[0][0]->push_back(value);
        return;
    }
    int v = (*value)[(*keyIndices)[keySize - height]];
    int type = 0;
    if (v >= halfMaxInt) {
        v -= halfMaxInt;
        type = 1;
    }
    //leaf array
    if (height==1) {
        if (!leafTuples[type].count(v)) {
            leafTuples[type][v] = new list<vector<int>* >();
        }
        leafTuples[type][v]->push_back(value);
    }//nested array
    else {
        if (!nestedArray[type].count(v)) {
            nestedArray[type][v] = new AuxiliaryMap(height - 1, keyIndices);
        }
        nestedArray[type][v]->insert2(value);
    }
}

list<vector<int>*>* AuxiliaryMap::getValues(vector<int>& tuple) {
    //TODO  can we do better?
    if(!keySize) {
        return leafTuples[0][0];
    }
    int index = tuple.size() - height;
    int v = tuple[index];
    int type = 0;
    if (v >= halfMaxInt) {
        v -= halfMaxInt;
        type = 1;
    }
    if (height == 1) {
        if(!leafTuples[type].count(v)) {
            return NULL;
        }
        return leafTuples[type][v];
    }
    if(!nestedArray[type].count(v)) {
        return NULL;
    }
    return nestedArray[type][v]->getValues(tuple);
}


