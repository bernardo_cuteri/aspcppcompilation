/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WQi.h
 * Author: bernardo
 *
 * Created on January 26, 2017, 2:29 PM
 */

#ifndef WQi_H
#define WQi_H

#include <list>
#include <vector>
#include <string>
#include <unordered_map>
#include <unordered_set>
using namespace std;

class WQi {
public:
    enum MatchingType: int_fast8_t{INT, STRING, BOTH};
    WQi(int height, int predicateAriety);
    void insert(vector<int> & value);
    virtual ~WQi();
    int size() const;
    void next(vector<int> & next) const;
    void pop(vector<int> & tuple);
    bool contains(vector<int> & tuple) const;
    void remove(vector<int> & tuple);
    void printTree() const;
    void printTuples(const string & predicateName) const;
    int getTupleCount() const;
protected:
    void printTuples(const string & prec, bool flag) const;
    unordered_map<int,WQi*> nestedArray[2];
    vector<int> definedIdexes[2];
    unordered_set<int> leafTruth[2];
    int height;
    int predicateAriety;

};

#endif /* WQi_H */

