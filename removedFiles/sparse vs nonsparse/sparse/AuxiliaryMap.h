/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AuxiliaryMap.h
 * Author: bernardo
 *
 * Created on January 26, 2017, 2:54 PM
 */

#ifndef AuxiliaryMap_H
#define AuxiliaryMap_H
#include <list>
#include <vector>
#include <unordered_map>


using namespace std;

class AuxiliaryMap {
public:
    AuxiliaryMap();
    AuxiliaryMap(int height);
    AuxiliaryMap(int height, vector<int> * keyIndices);
    void insert2(vector<int>* value);
    virtual ~AuxiliaryMap();
    list<vector<int>*>* getValues(vector<int>& tuple);
protected:
    unordered_map<int, AuxiliaryMap*> nestedArray[2];
    unordered_map<int,list<vector<int>*>*> leafTuples[2];
    vector<int>* keyIndices;
    int height;
    int keySize;

};



#endif /* AuxiliaryMap_H */

