/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AuxiliaryMapSparse.h
 * Author: bernardo
 *
 * Created on January 26, 2017, 2:54 PM
 */

#ifndef AUXILIARYMAPSPARSE_H
#define AUXILIARYMAPSPARSE_H
#include <list>
#include <vector>
#include <unordered_map>


using namespace std;

class AuxiliaryMapSparse {
public:
    AuxiliaryMapSparse();
    AuxiliaryMapSparse(int height);
    AuxiliaryMapSparse(int height, vector<int> * keyIndices);
    void insert2(vector<int>* value);
    virtual ~AuxiliaryMapSparse();
    list<vector<int>*>* getValues(vector<int>& tuple);
protected:
    unordered_map<int, AuxiliaryMapSparse*> nestedArray[2];
    unordered_map<int,list<vector<int>*>*> leafTuples[2];
    vector<int>* keyIndices;
    int height;
    int keySize;

};



#endif /* AUXILIARYMAPSPARSE_H */

